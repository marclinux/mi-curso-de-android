package com.expea.curso.p04fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentoDos extends Fragment {
    TextView txvSaludo;

    public FragmentoDos() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragmento_dos,
                container, false);
        txvSaludo = view.findViewById(R.id.txvSaludo);
        return  view;
    }

    public void RecibeDatos(String mensaje) {
        txvSaludo.setText(mensaje);
    }
}
