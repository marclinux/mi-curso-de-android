package com.curso.expea.listaencurso;

/**
 * Created by marcos on 26/08/2015.
 */
public class clsProductos {

    private String Clave;

    private String Descripcion;

    private float Precio;

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public float getPrecio() {
        return Precio;
    }

    public void setPrecio(float precio) {
        Precio = precio;
    }
}
