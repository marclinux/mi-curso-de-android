package com.expea.exreferencia;

public class Referencia {
	private int _id;
	private String NombreArchivo;
	private double Latitud;
	private double Longitud;
	
	public String getNombreArchivo() {
		return NombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		NombreArchivo = nombreArchivo;
	}
	public double getLongitud() {
		return Longitud;
	}
	public void setLongitud(double longitud) {
		Longitud = longitud;
	}
	public double getLatitud() {
		return Latitud;
	}
	public void setLatitud(double latitud) {
		Latitud = latitud;
	}
	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}

}
