package com.curso.expea.listas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lstProductos;

    clsProductosAdapter customListAdapter;

    ArrayList<clsProductosPrecios> productos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        productos = new ArrayList<clsProductosPrecios>();
        clsProductosPrecios producto = new clsProductosPrecios();
        producto.setClaveProducto("P0011");
        producto.setDescripcionProd("REF COCA COLA 600 ML");
        producto.setPrecioUnit(11.5f);
        productos.add(producto);
        producto = new clsProductosPrecios();
        producto.setClaveProducto("P0041");
        producto.setDescripcionProd("REF FANTA 600 ML");
        producto.setPrecioUnit(10.0f);
        productos.add(producto);
        producto = new clsProductosPrecios();
        producto.setClaveProducto("P3071");
        producto.setDescripcionProd("REF MANZANITA SOL 600 ML");
        producto.setPrecioUnit(10.0f);
        productos.add(producto);
        lstProductos = (ListView) findViewById(R.id.lsvProductos);
        customListAdapter = new clsProductosAdapter(getApplicationContext(),
                R.layout.item_lista, productos);
        lstProductos.setAdapter(customListAdapter);
        lstProductos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String claveSeleccionada = ((TextView) view.findViewById(R.id.txvClave)).getText().toString();
                Toast.makeText(getApplicationContext(), claveSeleccionada, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
