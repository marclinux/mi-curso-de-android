package com.expea.curso.p03parcelable.clases;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class clsUsuarios implements Parcelable {
    private int id;
    private String login;
    private String password;
    private int Nivel;
    private int Activo;
    private int CambiaContrasenia;
    public static final Parcelable.Creator<clsUsuarios> CREATOR
            = new Parcelable.Creator<clsUsuarios>() {
        public clsUsuarios createFromParcel(Parcel in) {
            return new clsUsuarios(in);
        }

        public clsUsuarios[] newArray(int size) {
            return new clsUsuarios[size];
        }
    };
    public clsUsuarios() {
    }
    public clsUsuarios(Parcel in)
    {
        id = in.readInt();
        login = in.readString();
        password = in.readString();
        Nivel = in.readInt();
        Activo = in.readInt();
        CambiaContrasenia = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(login);
        dest.writeString(password);
        dest.writeInt(Nivel);
        dest.writeInt(Activo);
        dest.writeInt(CambiaContrasenia);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNivel() {
        return Nivel;
    }

    public void setNivel(int nivel) {
        Nivel = nivel;
    }

    public int getActivo() {
        return Activo;
    }

    public void setActivo(int activo) {
        Activo = activo;
    }

    public int getCambiaContrasenia() {
        return CambiaContrasenia;
    }

    public void setCambiaContrasenia(int cambiaContrasenia) {
        CambiaContrasenia = cambiaContrasenia;
    }

    public String ToString() {
        return "Id: " + String.valueOf(getId()) + " Login: " + getLogin() +
                " Password: " + getPassword() +
                " Nivel: " + String.valueOf(getNivel()) + "Activo: " +
                String.valueOf(getActivo()) + " Cambia Contraseña: " +
                String.valueOf(getCambiaContrasenia());
    }

    public List<clsUsuarios> getUsuarios()
    {
        ArrayList<clsUsuarios> Usuarios =
                new ArrayList<clsUsuarios>();
        clsUsuarios UsuarioLista = new clsUsuarios();
        UsuarioLista.setId(1);
        UsuarioLista.setLogin("mhernandez");
        UsuarioLista.setNivel(99);
        Usuarios.add(UsuarioLista);
        UsuarioLista = new clsUsuarios();
        UsuarioLista.setId(2);
        UsuarioLista.setLogin("pedroperez");
        UsuarioLista.setNivel(60);
        Usuarios.add(UsuarioLista);
        UsuarioLista = new clsUsuarios();
        UsuarioLista.setId(3);
        UsuarioLista.setLogin("mariagomez");
        UsuarioLista.setNivel(20);
        Usuarios.add(UsuarioLista);
        return Usuarios;
    }
}
