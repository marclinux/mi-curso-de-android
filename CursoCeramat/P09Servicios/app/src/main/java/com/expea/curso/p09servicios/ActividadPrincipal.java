package com.expea.curso.p09servicios;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity {
    Button btnIiciar;
    Button btnObtenFecha;
    Button btnDetener;
    TextView txvFecha;
    ServicioEjemplo servicio;
    boolean conectado = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        btnIiciar = findViewById(R.id.btnIniciar);
        btnObtenFecha = findViewById(R.id.btnVerFecha);
        btnDetener = findViewById(R.id.btnDetener);
        btnIiciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Servico Iniciado",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), ServicioEjemplo.class);
                startService(intent);
                bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            }
        });
        btnObtenFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(conectado)
                    txvFecha.setText(servicio.getFechaActual());
            }
        });
        btnDetener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(conectado)
                    unbindService(serviceConnection);
                Intent intent = new Intent(getApplicationContext(), ServicioEjemplo.class);
                stopService(intent);
            }
        });
    }
    private ServiceConnection serviceConnection =
        new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                ServicioEjemplo.MyBinder myBinder =
                        (ServicioEjemplo.MyBinder) service;
                servicio = myBinder.getService();
                conectado = true;
            }
            @Override
            public void onServiceDisconnected(ComponentName name) {
                conectado = false;
            }
        };
}
