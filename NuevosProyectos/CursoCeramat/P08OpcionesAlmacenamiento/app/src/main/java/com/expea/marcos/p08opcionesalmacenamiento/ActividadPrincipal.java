package com.expea.marcos.p08opcionesalmacenamiento;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class ActividadPrincipal extends AppCompatActivity {
    EditText edtValor;
    Button btnEscribirCache;
    Button btnLeerCache;
    Button btnEscribirExterno;
    Button btnLeerExterno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        edtValor = findViewById(R.id.edtValor);
        btnEscribirCache = findViewById(R.id.btnEscribirCache);
        btnLeerCache = findViewById(R.id.btnleerCache);
        btnEscribirExterno = findViewById(R.id.btnEscribirExterno);
        btnLeerExterno = findViewById(R.id.btnLeerExterno);
        AsignaListeners();
    }

    private void AsignaListeners() {
        btnEscribirExterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valor = edtValor.getText().toString();

                if (!TextUtils.isEmpty(valor)) {
                    File archivo = new File(getExternalFilesDir(DIRECTORY_DOWNLOADS), "mi_archivo_externo");
                    writeDataToFile(archivo, valor);
                }
            }
        });

        btnLeerExterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context ctx = getApplicationContext();
                File archivo = new File(getExternalFilesDir(DIRECTORY_DOWNLOADS), "mi_archivo_externo");
                String fileData = readFromFileInputStream(archivo);
                if (fileData.length() > 0) {
                    edtValor.setText(fileData);
                } else {
                    Toast.makeText(ctx, "Na hay datos.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Write to internal cache file button.
        btnEscribirCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valor = edtValor.getText().toString();
                if (!TextUtils.isEmpty(valor)) {
                    File file = new File(getCacheDir(), "mi_cache");
                    writeDataToFile(file, valor);
                }
            }
        });

        // Read from cache file.
        btnLeerCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Context ctx = getApplicationContext();

                    File cacheFileDir = new File(getCacheDir(), "mi_cache");

                    FileInputStream fileInputStream = new FileInputStream(cacheFileDir);

                    String fileData = readFromFileInputStream(fileInputStream);

                    if (fileData.length() > 0) {
                        edtValor.setText(fileData);
                    } else {
                        Toast.makeText(ctx, "No hay datos.", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    // This method will write data to file.
    private void writeDataToFile(File file, String data) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            this.writeDataToFile(fileOutputStream, data);
            fileOutputStream.close();
        } catch (FileNotFoundException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        } catch (IOException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // This method will write data to FileOutputStream.
    private void writeDataToFile(FileOutputStream fileOutputStream, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

            bufferedWriter.write(data);

            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
        } catch (FileNotFoundException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        } catch (IOException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // This method will read data from FileInputStream.
    private String readFromFileInputStream(FileInputStream fileInputStream) {
        StringBuffer retBuf = new StringBuffer();
        try {
            if (fileInputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String lineData = bufferedReader.readLine();
                while (lineData != null) {
                    retBuf.append(lineData);
                    lineData = bufferedReader.readLine();
                }
            }
        } catch (IOException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            return retBuf.toString();
        }
    }
    private String readFromFileInputStream(File file) {
        StringBuffer retBuf = new StringBuffer();
        try {
            if (file != null) {
                InputStreamReader inputStreamReader = new InputStreamReader( new FileInputStream(file));
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String lineData = bufferedReader.readLine();
                while (lineData != null) {
                    retBuf.append(lineData);
                    lineData = bufferedReader.readLine();
                }
            }
        } catch (IOException ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            return retBuf.toString();
        }
    }
}
