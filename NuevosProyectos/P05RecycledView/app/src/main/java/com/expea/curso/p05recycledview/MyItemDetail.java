package com.expea.curso.p05recycledview;

import com.expea.curso.p05recycledview.clases.clsUsuarios;

import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;

class MyItemDetail extends ItemDetailsLookup.ItemDetails {
    private final int adapterPosition;
    private final clsUsuarios selectionKey;
    public MyItemDetail(int adapterPosition, Object p1) {
        this.adapterPosition = adapterPosition;
        selectionKey = (clsUsuarios) p1;
    }

    @Override
    public int getPosition() {
        return adapterPosition;
    }

    @Nullable
    @Override
    public Object getSelectionKey() {
        return selectionKey.getId();
    }
}
