package com.expea.marcos.p10servicio;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActividadPrincipal extends AppCompatActivity {
    Button btnObtenerFecha;
    Button btnTerminaServicio;
    TextView txvFecha;
    ServicioEjemplo servicio;
    boolean mServiceBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        btnObtenerFecha = findViewById(R.id.btnObtenerFecha);
        btnTerminaServicio = findViewById(R.id.btnTerminaServicio);
        txvFecha = findViewById(R.id.txvFecha);
        btnObtenerFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mServiceBound)
                    txvFecha.setText(servicio.getFechaSistema());
            }
        });
        btnTerminaServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mServiceBound) {
                    unbindService(mServiceConnection);
                    mServiceBound = false;
                }
                Intent intent = new Intent(ActividadPrincipal.this,
                        ServicioEjemplo.class);
                stopService(intent);
            }
        });
        Intent intent = new Intent(ActividadPrincipal.this, ServicioEjemplo.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioEjemplo.MyBinder myBinder = (ServicioEjemplo.MyBinder) service;
            servicio = myBinder.getService();
            mServiceBound = true;
        }
    };
}
