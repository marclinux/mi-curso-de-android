package com.example.multiplesactividades;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class PrincipalActivity extends AppCompatActivity implements OnMapReadyCallback {
    TextView txvNombre;
    TextView txvNivel;
    clsUsuarios Usuario;
    Toolbar menuPrincipal;
    SupportMapFragment Fragmento;
    ArrayList<clsSitios> Sitios;
    GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        txvNombre = findViewById(R.id.txvNombre);
        txvNivel = findViewById(R.id.txvNivel);

        menuPrincipal = findViewById(R.id.mnuPrincipal);
        Usuario = new clsUsuarios();

        Intent intent = getIntent();
        Usuario = (clsUsuarios)intent.getParcelableExtra("usuario");
        txvNombre.setText(Usuario.getNombre());
        txvNivel.setText(String.valueOf(Usuario.getNivel()));
        Fragmento = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Fragmento.getMapAsync((OnMapReadyCallback) this);
        Sitios = new ArrayList<clsSitios>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean regresa = super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return regresa;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.mnuLista:
                intent = new Intent(this, ActividadLista.class);
                startActivityForResult(intent, 500);
                return true;
            case R.id.mnuAgregar:
                intent = new Intent(this, AgregaSitio.class);
                startActivityForResult(intent, 510);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 500) {
            if (resultCode == RESULT_OK) {

                String sidUsuario = data.getStringExtra("idUsuario");

                Toast.makeText(getApplicationContext(), sidUsuario,
                        Toast.LENGTH_LONG).show();
            }
        }
        else if(requestCode == 510) {
            if(resultCode == RESULT_OK) {
                Fragmento.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        MyAsyncTask tarea = new MyAsyncTask();
                        tarea.execute("");
                    }
                });

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
       // mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng cuevaDelJaguar = new LatLng(16.73, -93.08);
        mMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(cuevaDelJaguar, 14));
        MyAsyncTask tarea = new MyAsyncTask();
        tarea.execute("");
    }

    private class MyAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            AppDataBase bd = AppDataBase.getDatabase(getApplicationContext());
            SitiosDao oSitiosDao = bd.SitiosDao();
            Sitios = (ArrayList<clsSitios>)oSitiosDao.ObtenTodos();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            AgregaSitios();
        }

        private void AgregaSitios()
        {
            for (clsSitios sitio: Sitios
                    ) {
                LatLng posicion = new LatLng(sitio.getLatitud(), sitio.getLongitud());
                Marker marcador =
                        mMap.addMarker(new MarkerOptions()
                                .position(posicion)
                                .title(sitio.getNombreSitio()));
                marcador.showInfoWindow();
            }

        }
    }
}
