package com.curso.expea.cuadriculas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GridView grdPedidos;

    ArrayList<clsPedidos> pedidos;

    clsPedidos pedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pedidos = new ArrayList<clsPedidos>();
        LlenaPedidos();
        grdPedidos = (GridView) findViewById(R.id.grdPedidos);
        clsGridAdapter gridAdapter = new clsGridAdapter(getApplicationContext(),
                R.layout.celda_grid, pedidos);
        grdPedidos.setAdapter(gridAdapter);

        grdPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                clsPedidos tpedido = new clsPedidos();
                tpedido = (clsPedidos) pedidos.get(i);
                Toast.makeText(getApplicationContext(), tpedido.getNombreCliente() , Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void LlenaPedidos() {
        pedido = new clsPedidos();
        pedido.setIdPedido(23);
        pedido.setFecha(new Date());
        pedido.setNombreCliente("PEDRO PEREZ");
        pedido.setNivelUsuario(1);
        pedidos.add(pedido);
        pedido = new clsPedidos();
        pedido.setIdPedido(24);
        pedido.setFecha(new Date());
        pedido.setNombreCliente("ALBERTO MARTINEZ");
        pedido.setNivelUsuario(2);
        pedidos.add(pedido);
        pedido = new clsPedidos();
        pedido.setIdPedido(25);
        pedido.setFecha(new Date());
        pedido.setNombreCliente("JULIAN LOPEZ");
        pedido.setNivelUsuario(1);
        pedidos.add(pedido);
    }
}
