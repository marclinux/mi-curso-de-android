package com.example.multiplesactividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.OnItemActivatedListener;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActividadLista extends AppCompatActivity {
    RecyclerView lstUsuarios;
    MiAdaptador Adaptador;
    RecyclerView.LayoutManager AdminLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_lista);
        lstUsuarios = findViewById(R.id.lstUsuarios);
        lstUsuarios.setHasFixedSize(true);

        // use a linear layout manager
        AdminLayout = new LinearLayoutManager(this);
        lstUsuarios.setLayoutManager(AdminLayout);

        clsUsuarios Usuario = new clsUsuarios();
        ArrayList<clsUsuarios> UsuariosLista = new ArrayList<clsUsuarios>();
        UsuariosLista = Usuario.ObtenTodos();
        // specify an adapter (see also next example)
        Adaptador = new MiAdaptador(UsuariosLista);
        lstUsuarios.setAdapter(Adaptador);

        SelectionTracker traker = new SelectionTracker.Builder<>(
                "my-selection-id",
                lstUsuarios,
                new StableIdKeyProvider(lstUsuarios),
                new MyDetailsLookup(lstUsuarios),
                StorageStrategy.createLongStorage())
                .withOnItemActivatedListener(new OnItemActivatedListener<Long>() {
                    @Override
                    public boolean onItemActivated(@NonNull ItemDetailsLookup.ItemDetails<Long> item, @NonNull MotionEvent e) {
                        Intent intent = new Intent();
                        intent.putExtra("idUsuario", String.valueOf(item.getSelectionKey()));
                        setResult(RESULT_OK, intent);
                        finish();
                        return true;
                    }
                })
                .build();
        Adaptador.setSelectionTracker(traker);
    }
}
