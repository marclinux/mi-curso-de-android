package com.expea.curso.p13volley;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expea.curso.p13volley.clases.clsUsuarios;

import java.util.ArrayList;

public class UsuariosAdapter extends ArrayAdapter<clsUsuarios> {
    private int layoutResourceId;
    public UsuariosAdapter(Context context, int resource,
                           ArrayList<clsUsuarios> items) {
        super(context, resource, items);
        layoutResourceId = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,
                        @NonNull ViewGroup parent) {
        View v = convertView;
        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(this.layoutResourceId, parent, false);
        }
        clsUsuarios p = new clsUsuarios();
        p = getItem(position);
        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.txvCliente);
            TextView tt2 = (TextView) v.findViewById(R.id.txvNombre);
            if (tt1 != null) {
                tt1.setText(p.getCliente());
            }
            if (tt2 != null) {
                tt2.setText(p.getNombre());
            }
        }
        return v;
    }
}
