package com.example.multiplesactividades;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

public class MiAdaptador extends RecyclerView.Adapter<MiAdaptador.MiViewHolder> {
    private ArrayList<clsUsuarios> mDataset;
    private SelectionTracker selectionTracker;

    public MiAdaptador(ArrayList<clsUsuarios> myDataset) {
        mDataset = myDataset;
    }
    public SelectionTracker getSelectionTracker() {
        return selectionTracker;
    }
    public void setSelectionTracker(SelectionTracker selectionTracker) {
        this.selectionTracker = selectionTracker;
    }
    public class MiViewHolder extends RecyclerView.ViewHolder {
        private TextView txvNombre;
        private TextView txvNivel;

        public MiViewHolder(@NonNull View itemView) {
            super(itemView);
            setTxvNombre((TextView)itemView.findViewById(R.id.txvNombre));
            setTxvNivel((TextView)itemView.findViewById(R.id.txvNivel));
        }

        public TextView getTxvNombre() {
            return txvNombre;
        }

        public void setTxvNombre(TextView txvNombre) {
            this.txvNombre = txvNombre;
        }

        public TextView getTxvNivel() {
            return txvNivel;
        }

        public void setTxvNivel(TextView txvNivel) {
            this.txvNivel = txvNivel;
        }

        public ItemDetailsLookup.ItemDetails getItemDetails() {
            return new MyItemDetail(getAdapterPosition(), mDataset.get(getAdapterPosition()));
        }
    }

    @NonNull
    @Override
    public MiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista, parent, false);
        MiViewHolder vh = new MiViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MiViewHolder holder, int position) {
        holder.getTxvNombre().setText(mDataset.get(position).getNombre());
        holder.getTxvNivel().setText(String.valueOf(mDataset.get(position).getNivel()));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
