package com.expea.curso.p13volley.clases;

import java.util.ArrayList;

public class consultaUsuarios {
    private boolean estatus;
    private ArrayList<clsUsuarios> resultado;

    public boolean isEstatus() {
        return estatus;
    }

    public void setEstatus(boolean estatus) {
        this.estatus = estatus;
    }

    public ArrayList<clsUsuarios> getResultado() {
        return resultado;
    }

    public void setResultado(ArrayList<clsUsuarios> resultado) {
        this.resultado = resultado;
    }
}
