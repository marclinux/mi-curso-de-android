package com.expea.exreferencia;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class FabricaDatos extends SQLiteOpenHelper {

	private static String DB_NAME = "dbExReferencia.db";

	public static final int DATABASE_VERSION = 1;

	public static final String SCRIPT_CREACION_REF =
			"CREATE TABLE `Referencias` (\n" +
					"\t`_id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
					"\t`NombreArchivo`\tTEXT,\n" +
					"\t`Latitud`\tREAL,\n" +
					"\t`Longitud`\tREAL,\n" +
					"\t`Fecha`\tTEXT,\n" +
					"\t`Upload`\tINTEGER\n" +
					")";

	public static final String SCRIPT_ELIMINAR_REF = "";

	private final Context myContext;

	SQLiteDatabase bd;
	/**
	* Constructor
	* Toma referencia hacia el contexto de la aplicación que lo invoca
	* Crea un objeto DBOpenHelper que nos permitirá controlar la apertura de la base de datos.
	* @param context
	*/
	public FabricaDatos(Context context) {
	 super(context, DB_NAME, null, 1);
	 this.myContext = context;	 
	}

	@Override
	public void onCreate(SQLiteDatabase sqLitedb) {
		sqLitedb.execSQL(SCRIPT_CREACION_REF);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLitedb, int i, int i1) {
		sqLitedb.execSQL(SCRIPT_ELIMINAR_REF);
		onCreate(sqLitedb);
	}

	public void open() throws SQLException{
		 
		bd = this.getWritableDatabase();
		 
	}
	
	@Override
	public synchronized void close() {
		if (bd != null)
			bd.close();
		super.close();
	}
	 
	public long Insertar(String tabla, ContentValues valores)
	{
		return bd.insert(tabla, null, valores);
	}
	
	public boolean Actualizar(String tabla, ContentValues valores, String campo, int valor)
	{
		return bd.update(tabla, valores, campo + "=" + valor, null) > 0;
	}
}
