﻿package EntidadAsesor;

public class clsProductosMovs extends clsProductosPrecios {
	private float fCantidad;

	public final float getCantidad() {
		return fCantidad;
	}

	public final void setCantidad(float value) {
		fCantidad = value;
	}

	private float fMontoIVA;

	public final float getMontoIVA() {
		return fMontoIVA;
	}

	public final void setMontoIVA(float value) {
		fMontoIVA = value;
	}

	private int fCambioPrecio;

	public final int getCambioPrecio() {
		return fCambioPrecio;
	}

	public final void setCambioPrecio(int value) {
		fCambioPrecio = value;
	}

	private float fExistenciasEnDoc;

	public final float getFExistenciasEnDoc() {
		return fExistenciasEnDoc;
	}

	public final void setFExistenciasEnDoc(float value) {
		fExistenciasEnDoc = value;
	}
	
	private float Descuento;

	public float getDescuento() {
		return Descuento;
	}

	public void setDescuento(float Descuento) {
		this.Descuento = Descuento;
	}
	
	public float getTotal() {
		return fCantidad * getPrecioUnit() + fMontoIVA - Descuento;
	}

}