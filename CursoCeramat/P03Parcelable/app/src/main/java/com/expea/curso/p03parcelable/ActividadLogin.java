package com.expea.curso.p03parcelable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.expea.curso.p03parcelable.clases.clsUsuarios;
public class ActividadLogin extends AppCompatActivity {
    clsUsuarios UsuarioSesion;
    EditText edtLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_login);
        edtLogin = (EditText) findViewById(R.id.edtLogin);
        UsuarioSesion = new clsUsuarios();
    }
    public void btnAceptarOnClick(View view) {
        Intent intent = new Intent(getApplicationContext(),
                ActividadPrincipal.class);
        UsuarioSesion.setId(1);
        UsuarioSesion.setLogin(edtLogin.getText().toString());
        UsuarioSesion.setNivel(99);
        intent.putExtra("usuarioSesion", UsuarioSesion);
        startActivity(intent);
    }
}
