package com.expea.curso.p03layoutsavanzados.Clases;

import android.os.Parcel;
import android.os.Parcelable;

public class clsUsuarios implements Parcelable {
    private int id;
    private  String login;
    private  String password;
    private  int nivel;
    private int activo;
    private  int cambiarContrasenia;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Object createFromParcel(Parcel source) {
            return new clsUsuarios(source);
        }

        @Override
        public Object[] newArray(int size) {
            return new Object[size];
        }
    };

    public clsUsuarios() {

    }

    public clsUsuarios(Parcel in) {
        this.id = in.readInt();
        this.login = in.readString();
        this.password = in.readString();
        this.nivel = in.readInt();
        this.activo = in.readInt();
        this.cambiarContrasenia = in.readInt();
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getCambiarContrasenia() {
        return cambiarContrasenia;
    }

    public void setCambiarContrasenia(int cambiarContrasenia) {
        this.cambiarContrasenia = cambiarContrasenia;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(login);
        dest.writeString(password);
        dest.writeInt(nivel);
        dest.writeInt(activo);
        dest.writeInt(cambiarContrasenia);
    }
}
