package com.curso.expea.basedatossqlite;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    ListView lsvContactos;

    ArrayList<clsContactos> contactos;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        lsvContactos = (ListView) v.findViewById(R.id.lsvContactos);
        contactos = new ArrayList<clsContactos>();
        clsBaseDatos bd = ((MainActivity)getActivity()).getBd();
        clsDatosContactos dcon = new clsDatosContactos();
        dcon.LlenaContactos(bd, contactos);
        ContactosAdapter cAdapter = new ContactosAdapter(v.getContext(), R.layout.item_lista, contactos);
        return v;
    }
}
