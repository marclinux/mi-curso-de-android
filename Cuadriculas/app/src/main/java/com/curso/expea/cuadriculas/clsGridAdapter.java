package com.curso.expea.cuadriculas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by marcos on 24/08/2015.
 */
public class clsGridAdapter extends ArrayAdapter<clsPedidos> {

    private int layoutResourceId;
    public clsGridAdapter(Context context, int resource, ArrayList<clsPedidos> objects) {
        super(context, resource, objects);
        layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(this.layoutResourceId, parent, false);
        }

        clsPedidos pedido = new clsPedidos();

        pedido = getItem(position);

        if(pedido != null) {
            TextView txtIdPedido = (TextView) v.findViewById(R.id.txtIdPedido);
            TextView txtFecha = (TextView) v.findViewById(R.id.txtFecha);
            TextView txtNombreCliente = (TextView) v.findViewById(R.id.txtNombreCliente);
            if(txtIdPedido != null)
                txtIdPedido.setText(String.valueOf(pedido.getIdPedido()));
            if(txtFecha != null)
                txtFecha.setText(String.valueOf(pedido.getFecha()));
            if(txtNombreCliente != null)
                txtNombreCliente.setText(pedido.getNombreCliente());
            if(pedido.getNivelUsuario() == 1)
                v.setBackgroundColor(0x30FF0000);
            else
                v.setBackgroundColor( 0x300000FF);}
        return v;
    }
}
