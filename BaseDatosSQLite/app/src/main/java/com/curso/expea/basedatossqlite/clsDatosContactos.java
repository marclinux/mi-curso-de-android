package com.curso.expea.basedatossqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by marcos on 27/08/2015.
 */
public class clsDatosContactos {

    private final String CONSULTA_CONTACTOS =
            "SELECT id, NombreContacto, NumeroTelefonico " +
            "FROM MisContactos";

    public void LlenaContactos(clsBaseDatos bd,
                                ArrayList<clsContactos> contactos) {
        clsContactos con;
        Cursor cr = bd.Consulta(CONSULTA_CONTACTOS);
        contactos.clear();
        while (cr.moveToNext()) {
            con = new clsContactos();
            con.setId(cr.getInt(0));
            con.setNombreCompleto(cr.getString(1));
            con.setNumeroTelefonico(cr.getString(2));
            contactos.add(con);
        }
        cr.close();
    }

    public void InsertaContacto(clsBaseDatos bd,
                                String Nombre, String Numero) {
        ContentValues cv = new ContentValues();
        cv.put("NombreCompleto", Nombre);
        cv.put("NumeroTelefonico", Numero);
        bd.Inserta(cv);
    }
}
