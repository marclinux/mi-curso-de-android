package com.expea.curso.p01actividadsimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class ActividadPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.w("MyApp", "Se inicio la actividad");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.w("MyApp", "Se detuvo la actividad");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.w("MyApp", "Se resumio la actividad");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.w("MyApp", "Se destruyó la actividad");
    }
}
