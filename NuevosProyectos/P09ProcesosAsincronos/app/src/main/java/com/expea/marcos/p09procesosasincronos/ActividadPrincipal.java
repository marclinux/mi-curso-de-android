package com.expea.marcos.p09procesosasincronos;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ActividadPrincipal extends AppCompatActivity {
    private ProgressBar asyncProgress;
    private TextView txvResultado;
    private Button btnIniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        txvResultado = findViewById(R.id.txvResultado);
        asyncProgress = findViewById(R.id.pgrbTarea);
        btnIniciar = findViewById(R.id.btnIniciar);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = "Hola mundo, desde Chiapas";
                MyAsyncTask Tarea = new MyAsyncTask();
                Tarea.execute(input);
            }
        });
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... parameter) {
            // Moved to a background Thread.
            String result = "";
            int myProgress = 0;
            int inputLength = parameter[0].length();

            // Perform background processing task, update myProgress]
            for (int i = 1; i <= inputLength; i++) {
                myProgress = i;
                result = result + parameter[0].charAt(inputLength - i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                publishProgress(myProgress);
            }

            // Return the value to be passed to onPostExecute
            return result;
        }

        @Override
        protected void onPreExecute() {
            // Synchronized to UI Thread.
            // Update the UI to indicate that background loading is occurring
            asyncProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Synchronized to UI Thread.
            // Update progress bar, Notification, or other UI elements
            asyncProgress.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            // Synchronized to UI Thread.
            // Report results via UI update, Dialog, or Notifications
            asyncProgress.setVisibility(View.GONE);
            txvResultado.setText(result);
        }
    }

}
