package com.expea.disponibilidad;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button btnAceptar;
    TextView txvResultado;
    int contador = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnAceptar = findViewById(R.id.btnAceptar);
        txvResultado = findViewById(R.id.txvResutado);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Password correcto",
                        Toast.LENGTH_LONG).show();
                String mensaje = String.valueOf(contador);
                txvResultado.setText(mensaje);
                contador++;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.w("MiAplicacion", "La Actividad se inicio", null);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.w("MiAplicacion", "La Actividad se detuvo", null);
        Toast.makeText(this, "La actividad se detuvo",
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.w("MiAplicacion", "La Actividad se destruyo", null);
    }

    /*public void onClick(View view) {
        Toast.makeText(this, "Password correcto",
                Toast.LENGTH_LONG).show();
    }*/

}
