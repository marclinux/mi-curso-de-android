package com.expea.curso.p03parcelable;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

import com.expea.curso.p03parcelable.clases.clsUsuarios;

public class ActividadCreaUsuario extends AppCompatActivity {
    clsUsuarios UsuarioCrea;
    EditText edtLogin;
    EditText edtPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_crea_usuario);
        edtLogin = findViewById(R.id.edtLogin);
        edtPassword = findViewById((R.id.edtPassword));
        UsuarioCrea = new clsUsuarios();
    }

    public void NivelClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.rbtnAdministrador:
                if(checked)
                    UsuarioCrea.setNivel(99);
                break;
            case R.id.rbtnSupervisor:
                if(checked)
                    UsuarioCrea.setNivel(60);
                break;
            case R.id.rbtnVendedor:
                if(checked)
                    UsuarioCrea.setNivel(20);
                break;
        }
    }
    public void ChecksClick(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.chkActivo:
                if(checked)
                    UsuarioCrea.setActivo(1);
                else
                    UsuarioCrea.setActivo(0);
                break;
            case R.id.chkCapturaContrasenia:
                if(checked)
                    UsuarioCrea.setCambiaContrasenia(1);
                else
                    UsuarioCrea.setCambiaContrasenia(0);
                break;
        }
    }

    public void GuardarClick(View view) {
        UsuarioCrea.setLogin(edtLogin.getText().toString());
        UsuarioCrea.setPassword(edtPassword.getText().toString());
        AlertDialog.Builder dialog = new AlertDialog.Builder(
                ActividadCreaUsuario.this);
        dialog.setTitle("Datos del usuario");
        dialog.setMessage(UsuarioCrea.ToString());
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialog.show();
    }

    public void CancelarClick(View view) {
        finish();
    }
}
