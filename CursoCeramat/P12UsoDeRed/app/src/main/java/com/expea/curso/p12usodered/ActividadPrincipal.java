package com.expea.curso.p12usodered;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvResultado;
    Button btnIniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        iniciaVistas();
    }

    private void iniciaVistas() {
        txvResultado = findViewById(R.id.txvRespuesta);
        btnIniciar = findViewById(R.id.btnIniciar);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAsyncTask tarea = new MyAsyncTask();
                tarea.execute("");
            }
        });
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... strings) {
            URL url = null;
            HttpsURLConnection conexion = null;
            try {
                url = new URL("https://uinames.com/api?region=Mexico");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                conexion = (HttpsURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                conexion.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            conexion.setConnectTimeout(10000);
            conexion.setReadTimeout(10000);
            InputStream stream = null;
            try {
                stream = conexion.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            InputStreamReader iReader = new InputStreamReader(stream);
            BufferedReader bReader = new BufferedReader(iReader);
            StringBuffer sBuffer = new StringBuffer();
            String linea = null;
            try {
                linea = bReader.readLine();
                while (linea != null) {
                    sBuffer.append(linea);
                    linea = bReader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            linea = sBuffer.toString();
            return linea;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected void onPostExecute(String s) {
            try {
                //JSONArray
                JSONObject persona = new JSONObject(s);
                if (persona != null)
                    txvResultado.setText("Nombre : " + persona.getString("name") +
                            " Apellido: " + persona.getString("surname") +
                            " Genero: " + persona.getString("gender") +
                            " Region : " + persona.getString("region"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
