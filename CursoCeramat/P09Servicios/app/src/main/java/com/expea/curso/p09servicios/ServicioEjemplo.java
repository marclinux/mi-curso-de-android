package com.expea.curso.p09servicios;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.Date;

public class ServicioEjemplo extends Service {
    IBinder mBinder = new MyBinder();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(), "Servicio terminado",
                Toast.LENGTH_LONG).show();
    }
    public String getFechaActual() {
        Date fecha = new Date();
        return  fecha.toString();
    }
    public class MyBinder extends Binder {
        ServicioEjemplo getService() {
            return ServicioEjemplo.this;
        }
    }
}
