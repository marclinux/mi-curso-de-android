package com.example.multiplesactividades;

import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;

class MyItemDetail extends ItemDetailsLookup.ItemDetails {
    private final int adapterPosition;
    private final clsUsuarios selectionKey;
    public MyItemDetail(int adapterPosition, clsUsuarios usuario) {
        this.adapterPosition = adapterPosition;
        selectionKey = (clsUsuarios) usuario;
    }

    @Override
    public int getPosition() {
        return adapterPosition;
    }

    @Nullable
    @Override
    public Object getSelectionKey() {
        return selectionKey.getIdUsuario();
    }
}
