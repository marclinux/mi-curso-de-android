package com.ati.asesormovil;

import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import EntidadAsesor.clsUsuarios;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private Button btnAceptar;

	private TextView txtUsuario;
	private TextView txtPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnAceptar = (Button) findViewById(R.id.btnAceptar);
		txtUsuario = (TextView) findViewById(R.id.edtUsuario);
		txtPassword = (TextView) findViewById(R.id.edtPassword);
		
		Global.mRequestQueue = Volley.newRequestQueue(this);
		
		btnAceptar.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				ObtenerConfig();
				RevisaAcceso();
				return false;
			}
		});

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.Configuracion:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void RevisaAcceso() {
		if (txtUsuario.getText().toString().trim().length() != 0 
			&& txtPassword.getText().toString().trim().length() != 0) {
			String url = String.format("http://" + Global.Servidor +
										"/WebServAsesor/index.php/Acceso/usuario?login='%1$s'&password='%2$s'",
										txtUsuario.getText().toString(),
										txtPassword.getText().toString());
			JsonObjectRequest jr = new JsonObjectRequest(Request.Method.GET, url,
					null, new Response.Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject response) {
							Global.UsuarioSesion = new clsUsuarios();
							Global.UsuarioSesion.setIdUsuario(response.optInt("idUsuario"));
							Global.UsuarioSesion.setNombreCompleto(response.optString("NombreUsu"));
							Global.UsuarioSesion.setLogin(response.optString("LoginUsu"));
							Global.UsuarioSesion.setNivel(response.optInt("nivel"));
							if(Global.UsuarioSesion.getNivel() > 0)
								AbrirPedidos();
						}
					}, new Response.ErrorListener() {
					    @Override
					    public void onErrorResponse(VolleyError error) {
					        //VolleyLog.e("Error: ", error.getMessage());
					    	Toast tx = Toast.makeText(getApplicationContext(),
									"Ocurrio el siguiente error: " +
									error.getMessage(), Toast.LENGTH_LONG);
							tx.show();
					    }
					});
			Global.mRequestQueue.add(jr);
			
		} else {
			Toast tx = Toast.makeText(getApplicationContext(),
					"Faltan valores", Toast.LENGTH_LONG);
			tx.show();
		}
	}
                                 
	public void AbrirPedidos() {
		txtUsuario.setText("");
		txtPassword.setText("");
		Intent intent = new Intent(this, PedidosActivity.class);
		startActivity(intent);		
	}
	
	public void ObtenerConfig() {
		SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(this);

		Global.Servidor = SP.getString("Servidor", "localhost");
		Global.BaseDatos = SP.getString("BaseDatos", "tejidospuebla");
	}

}
