package com.expea.curso.p12clientewebservice;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ActividadPrincipal extends AppCompatActivity {
    Button btnIniciar;
    TextView txvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        btnIniciar = findViewById(R.id.btnIniciar);
        txvResultado = findViewById(R.id.txvResultado);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAsyncTask tarea = new MyAsyncTask();
                tarea.execute("");
            }
        });
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... strings) {
            String regresa = "";
            HttpURLConnection httpConn = null;
            InputStreamReader isReader;
            BufferedReader bufReader = null;
            StringBuffer readTextBuf = new StringBuffer();
            URL url = null;
            try {
                url = new URL("http://uinames.com/api?region=Mexico");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                httpConn = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                httpConn.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            httpConn.setConnectTimeout(10000);
            httpConn.setReadTimeout(10000);
            InputStream inputStream;
            try {
                inputStream = httpConn.getInputStream();
                isReader = new InputStreamReader(inputStream);
                bufReader = new BufferedReader(isReader);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String line = "";
            try {
                line = bufReader.readLine();
                while (line != null) {
                    readTextBuf.append(line);
                    line = bufReader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            regresa = readTextBuf.toString();
            return regresa;
        }

        @Override
        protected void onPostExecute(String s) {
            JSONObject nombre = null;
            try {
                nombre = new JSONObject(s);
                if (nombre != null) {
                    txvResultado.setText("Nombre : "+ nombre.getString("name") +
                            " Apellido: " + nombre.getString("surname") +
                            " Genero: " + nombre.getString("gender") +
                            " Region : " + nombre.getString("region"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

