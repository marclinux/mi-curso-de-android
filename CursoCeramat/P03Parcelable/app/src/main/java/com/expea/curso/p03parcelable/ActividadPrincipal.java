package com.expea.curso.p03parcelable;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.expea.curso.p03parcelable.clases.*;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvLogin;
    TextView txvNIvel;
    clsUsuarios UsuarioSesion;
    Toolbar tlbPrincipal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        tlbPrincipal = findViewById(R.id.tlbPrincipal);
        setSupportActionBar(tlbPrincipal);
        txvLogin = findViewById(R.id.txvLogin);
        txvNIvel = findViewById(R.id.txvNivel);
        UsuarioSesion = new clsUsuarios();
        Bundle data = getIntent().getExtras();
        UsuarioSesion = data.getParcelable("usuarioSesion");
        txvLogin.setText(UsuarioSesion.getLogin());
        txvNIvel.setText(String.valueOf(UsuarioSesion.getNivel()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_favorite:
                intent = new Intent(ActividadPrincipal.this,
                        ActividadCreaUsuario.class);
                startActivity(intent);
                break;
            case R.id.action_Consulta:
                intent = new Intent(ActividadPrincipal.this,
                        ActividadConsulta.class);
                startActivityForResult(intent, 50);
                break;
        }
        return true;
    }

    public void ConsultaClick(View view) {
        Intent intent = new Intent(ActividadPrincipal.this,
                                    ActividadConsulta.class);
        startActivityForResult(intent, 50);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 50)
        {
            String message = data.getStringExtra("idConsultado");
            Toast.makeText(ActividadPrincipal.this, message,
                    Toast.LENGTH_LONG).show();
        }
    }

    public void FloatingClick(View view) {
        Intent intent = new Intent(ActividadPrincipal.this,
                ActividadCreaUsuario.class);
        startActivity(intent);
    }
}
