package com.expea.curso.p01miprimeraaplicacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class ActividadPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.w("MyApp", "La actividad se inicio");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.w("MyApp", "La actividad se detuvo");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.w("MyApp", "La actividad se destruyo");
    }
}
