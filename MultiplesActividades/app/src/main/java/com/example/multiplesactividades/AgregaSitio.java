package com.example.multiplesactividades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregaSitio extends AppCompatActivity {

    EditText edtNombreSitio;
    EditText edtLatitud;
    EditText edtLongitud;
    Button btnGuardarSitio;
    Button btnObtenUbicacion;
    Location ultimaUbicacion = new Location("");
    LocationListener listener;
    LocationManager locManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_agrega_sitio);

        edtNombreSitio = findViewById(R.id.edtNombreSitio);
        edtLatitud = findViewById(R.id.edtLatitud);
        edtLongitud = findViewById((R.id.edtLongitud));
        btnGuardarSitio = findViewById(R.id.btnGuardaSitio);
        btnObtenUbicacion = findViewById(R.id.btnObtenUbicacion);

        btnGuardarSitio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAsyncTask tarea = new MyAsyncTask();
                tarea.execute("");            }
        });
        btnObtenUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ultimaUbicacion != null) {
                    edtLatitud.setText(String.valueOf(ultimaUbicacion.getLatitude()));
                    edtLongitud.setText(String.valueOf(ultimaUbicacion.getLongitude()));
                }
            }
        });
        IniciaLocationManager();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Criteria criterios = new Criteria();
                    criterios.setAccuracy(Criteria.ACCURACY_FINE);
                    criterios.setPowerRequirement(Criteria.POWER_MEDIUM);
                    criterios.setAltitudeRequired(false);
                    criterios.setSpeedRequired(false);
                    criterios.setCostAllowed(true);

                    String bestProvider = locManager.getBestProvider(criterios, true);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)
                        locManager.requestLocationUpdates(bestProvider /*LocationManager.GPS_PROVIDER*/, 0, 0, listener);

                } else {
                    Toast.makeText(getApplicationContext(), "No se acepto el permiso",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private class MyAsyncTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... strings) {
            AppDataBase bd = AppDataBase.getDatabase(getApplicationContext());
            SitiosDao oSitiosDao = bd.SitiosDao();
            clsSitios Sitio = new clsSitios();
            Sitio.setNombreSitio(edtNombreSitio.getText().toString());
            Sitio.setLatitud(Float.valueOf(edtLatitud.getText().toString()));
            Sitio.setLongitud(Float.valueOf(edtLongitud.getText().toString()));
            long i = oSitiosDao.insert(Sitio);
            return String.valueOf(i);
        }

        @Override
        protected void onPostExecute(String s) {
            if(s != null)
            {
                Toast.makeText(getApplicationContext(),
                        "Se agrego el registro: " + s, Toast.LENGTH_LONG)
                        .show();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
    private void IniciaLocationManager() {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ultimaUbicacion.set(location);
                Log.w("MiApplicacion", String.valueOf(location.getLongitude()));
                Log.w("MiApplicacion", String.valueOf(location.getLatitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            //return;
        }
        else
        {
            Criteria criterios = new Criteria();
            criterios.setAccuracy(Criteria.ACCURACY_FINE);
            criterios.setPowerRequirement(Criteria.POWER_MEDIUM);
            criterios.setAltitudeRequired(false);
            criterios.setSpeedRequired(false);
            criterios.setCostAllowed(true);

            String bestProvider = locManager.getBestProvider(criterios, true);
            locManager.requestLocationUpdates(bestProvider /*LocationManager.GPS_PROVIDER*/, 0, 0, listener);
        }
    }
}
