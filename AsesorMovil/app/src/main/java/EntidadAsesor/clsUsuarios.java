package EntidadAsesor;

public class clsUsuarios {
	
	private int idUsuario;
	
	private String NombreCompleto;
	
	private String Login;
	
	private String Password;
	
	private int Nivel;
	
	public clsUsuarios()
	{
		Nivel = 0;
		NombreCompleto = "";
		Login = "";
		Password = "";
	}

	public String getNombreCompleto() {
		return NombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		NombreCompleto = nombreCompleto;
	}

	public String getLogin() {
		return Login;
	}

	public void setLogin(String login) {
		Login = login;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getNivel() {
		return Nivel;
	}

	public void setNivel(int nivel) {
		Nivel = nivel;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

}
