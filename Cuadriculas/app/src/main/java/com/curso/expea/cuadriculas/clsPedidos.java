package com.curso.expea.cuadriculas;

import java.util.Date;

/**
 * Created by marcos on 24/08/2015.
 */
public class clsPedidos {

    private int idPedido;

    private String NombreCliente;

    private Date Fecha;

    private int NivelUsuario;

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        NombreCliente = nombreCliente;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public int getNivelUsuario() {
        return NivelUsuario;
    }

    public void setNivelUsuario(int nivelUsuario) {
        NivelUsuario = nivelUsuario;
    }
}
