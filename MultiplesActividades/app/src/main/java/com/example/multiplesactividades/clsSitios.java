package com.example.multiplesactividades;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import androidx.annotation.NonNull;


@Entity(tableName = "Sitios")
public class clsSitios {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "idSitio")
    private int idSitio;
    private String NombreSitio;
    private float Latitud;
    private float Longitud;

    @NonNull
    public int getIdSitio() {
        return idSitio;
    }

    public void setIdSitio(@NonNull int idSitio) {
        this.idSitio = idSitio;
    }

    public String getNombreSitio() {
        return NombreSitio;
    }

    public void setNombreSitio(String nombreSitio) {
        NombreSitio = nombreSitio;
    }

    public float getLatitud() {
        return Latitud;
    }

    public void setLatitud(float latitud) {
        Latitud = latitud;
    }

    public float getLongitud() {
        return Longitud;
    }

    public void setLongitud(float longitud) {
        Longitud = longitud;
    }
}
