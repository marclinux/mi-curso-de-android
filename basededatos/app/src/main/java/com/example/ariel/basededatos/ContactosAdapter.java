package com.example.ariel.basededatos;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ariel on 26/08/2015.
 */
public class ContactosAdapter extends ArrayAdapter<Contacto> {
    private int idResource;
    public ContactosAdapter(Context context, int resource, ArrayList<Contacto> objects) {
        super(context, resource, objects);
        idResource=resource;
    }

    @Override
    public  View getView(int position, View convertView, ViewGroup parent) {
        //View v= super.getView(position, convertView, parent);
        View v=convertView;
        if(v==null){
            //tomar los datos del layout
            LayoutInflater linflater=LayoutInflater.from(getContext());
            v= linflater.inflate(idResource,parent,false);
        }
        Contacto p;
        p=getItem(position);
        TextView txvUno=(TextView)v.findViewById(R.id.txvUno);
        TextView txvDos=(TextView)v.findViewById(R.id.txvDos);
        TextView txvTres=(TextView)v.findViewById(R.id.txvTres);
        txvUno.setText(p.getId()+"");
        txvDos.setText(p.getNombre());
        txvTres.setText(p.getTel());
        v.setBackgroundColor(Color.BLACK);
        return v;
    }
}
