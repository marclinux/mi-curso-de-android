package com.expea.curso.p06almacenamientoorientacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvValor;
    EditText edtValor;
    Button btnRecupera;
    int valorInicial = 16;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        txvValor = findViewById(R.id.txvValor);
        edtValor = findViewById(R.id.edtValor);
        btnRecupera = findViewById((R.id.btnRecupera));
        btnRecupera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valorInicial++;
                edtValor.setText(String.valueOf(valorInicial));
            }
        });
        if(savedInstanceState != null)
            if(savedInstanceState.containsKey("ValorInicial"))
                valorInicial = savedInstanceState.getInt("ValorInicial");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("ValorInicial", valorInicial);
    }
}
