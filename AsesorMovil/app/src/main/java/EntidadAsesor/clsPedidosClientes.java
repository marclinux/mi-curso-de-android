﻿package EntidadAsesor;

public class clsPedidosClientes {
	public clsPedidosClientes() {
		fCliente = new clsClientes();
		fDetalle = new colProductosMovs();
	}

	private clsClientes fCliente;

	public final clsClientes getCliente() {
		return fCliente;
	}

	public final void setCliente(clsClientes value) {
		fCliente = value;
	}

	private colProductosMovs fDetalle;

	public final colProductosMovs getDetalle() {
		return fDetalle;
	}

	public final void setDetalle(colProductosMovs value) {
		fDetalle = value;
	}

	private int fidPedidoCliente;

	public final int getidPedidoCliente() {
		return fidPedidoCliente;
	}

	public final void setidPedidoCliente(int value) {
		fidPedidoCliente = value;
	}

	private int fidCotizacion;

	public final int getidCotizacion() {
		return fidCotizacion;
	}

	public final void setidCotizacion(int value) {
		fidCotizacion = value;
	}

	private java.util.Date fFechaPedido = new java.util.Date(0);

	public final java.util.Date getFechaPedido() {
		return fFechaPedido;
	}

	public final void setFechaPedido(java.util.Date value) {
		fFechaPedido = value;
	}

	private float fMontoAnticipo;

	public final float getMontoAnticipo() {
		return fMontoAnticipo;
	}

	public final void setMontoAnticipo(float value) {
		fMontoAnticipo = value;
	}

	private float fMonSubPedClien;

	public final float getMonSubPedClien() {
		return fMonSubPedClien;
	}

	public final void setMonSubPedClien(float value) {
		fMonSubPedClien = value;
	}

	private float fMonIvaPedClien;

	public final float getMonIvaPedClien() {
		return fMonIvaPedClien;
	}

	public final void setMonIvaPedClien(float value) {
		fMonIvaPedClien = value;
	}

	private float fMonTotPedClien;

	public final float getMonTotPedClien() {
		return fMonTotPedClien;
	}

	public final void setMonTotPedClien(float value) {
		fMonTotPedClien = value;
	}

	private java.util.Date fFechaEntrega = new java.util.Date(0);

	public final java.util.Date getFechaEntrega() {
		return fFechaEntrega;
	}

	public final void setFechaEntrega(java.util.Date value) {
		fFechaEntrega = value;
	}

	private java.util.Date fFechaMaximaEntr = new java.util.Date(0);

	public final java.util.Date getFechaMaximaEntr() {
		return fFechaMaximaEntr;
	}

	public final void setFechaMaximaEntr(java.util.Date value) {
		fFechaMaximaEntr = value;
	}

	private String fObservaciones;

	public final String getObservaciones() {
		return fObservaciones;
	}

	public final void setObservaciones(String value) {
		fObservaciones = value;
	}

	private int fidUsuario;

	public final int getidUsuario() {
		return fidUsuario;
	}

	public final void setidUsuario(int value) {
		fidUsuario = value;
	}

	private int fTipoPrecio;

	public final int getTipoPrecio() {
		return fTipoPrecio;
	}

	public final void setTipoPrecio(int value) {
		fTipoPrecio = value;
	}

	private float fMontoDescuento;

	public final float getMontoDescuento() {
		return fMontoDescuento;
	}

	public final void setMontoDescuento(float value) {
		fMontoDescuento = value;
	}
}