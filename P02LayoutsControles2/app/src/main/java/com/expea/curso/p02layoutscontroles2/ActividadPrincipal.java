package com.expea.curso.p02layoutscontroles2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ActividadPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
    }

    public void Relativo(View view) {
        Intent intent = new Intent(getApplicationContext(),
                ActividadRelativa.class);
        startActivity(intent);
    }

    public void Grid(View view) {
        Intent intent = new Intent(getApplicationContext(),
                ActividadGrid.class);
        startActivity(intent);
    }

    public void Constraint(View view) {
        Intent intent = new Intent(getApplicationContext(),
                ActividadConstraint.class);
        startActivity(intent);
    }
}
