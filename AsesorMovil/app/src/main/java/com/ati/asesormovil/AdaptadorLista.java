package com.ati.asesormovil;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import EntidadAsesor.clsProductosMovs;

public class AdaptadorLista extends ArrayAdapter<clsProductosMovs> {

	private int layoutResourceId;

	public AdaptadorLista(Context context, int resource, List<clsProductosMovs> items) {
	    super(context, resource, items);
	    this.layoutResourceId = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

	    View v = convertView;

	    if (v == null) {

	        LayoutInflater li;
	        li = LayoutInflater.from(getContext());
	        v = li.inflate(this.layoutResourceId, parent, false);

	    }

	    clsProductosMovs p = new clsProductosMovs(); 
	    p = getItem(position);

	    if (p != null) {

	        TextView tt = (TextView) v.findViewById(R.id.txvCantidad);
	        TextView tt1 = (TextView) v.findViewById(R.id.txvClave);
	        TextView tt2 = (TextView) v.findViewById(R.id.txvDescripcion);
	        TextView tt3 = (TextView) v.findViewById(R.id.txvPrecio);
	        TextView tt4 = (TextView) v.findViewById(R.id.txvIva);
	        TextView tt5 = (TextView) v.findViewById(R.id.txvDescuento);
	        TextView tt6 = (TextView) v.findViewById(R.id.txvMonto);

	        if (tt != null) {
	            tt.setText(String.valueOf(p.getCantidad()));
	        }
	        if (tt1 != null) {

	            tt1.setText(p.getClaveProducto());
	        }
	        if (tt2 != null) {

	            tt2.setText(p.getDescripcionProd());
	        }
	        if (tt3 != null) {

	            tt3.setText(String.valueOf(p.getPrecioUnit()));
	        }
	        if (tt4 != null) {

	            tt4.setText(String.valueOf(p.getDescuento()));
	        }
	        if (tt5 != null) {

	            tt5.setText(String.valueOf(p.getMontoIVA()));
	        }
	        if (tt6 != null) {

	            tt6.setText(String.valueOf(p.getTotal()));
	        }
	    }
	    if(position % 2 == 0)
			v.setBackgroundColor(Color.WHITE);
		else
			v.setBackgroundColor(0xFFF6F4C0);
	    return v;
	}
}
