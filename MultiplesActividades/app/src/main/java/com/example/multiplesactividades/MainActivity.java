package com.example.multiplesactividades;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    EditText edtLogin;
    EditText edtPassword;
    Button btnAceptar;
    clsUsuarios Usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtLogin = findViewById(R.id.edtLogin);
        edtPassword = findViewById(R.id.edtPassword);
        btnAceptar = findViewById(R.id.btnAceptar);
        Usuario = new clsUsuarios();
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = edtLogin.getText().toString();
                String password = edtPassword.getText().toString();
                if(login != "" && password != "")
                {
                    Usuario.setLogin(login);
                    Usuario.Obtener(password);
                    Intent intent = new Intent(getApplicationContext(),
                            PrincipalActivity.class);
                    intent.putExtra("usuario", Usuario);
                    //intent.putExtra("usuario", (Serializable) Usuario);
                    startActivity(intent);
                }
            }
        });
    }
}
