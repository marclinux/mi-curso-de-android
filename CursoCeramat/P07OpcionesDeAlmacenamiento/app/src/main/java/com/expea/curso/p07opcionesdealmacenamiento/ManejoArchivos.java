package com.expea.curso.p07opcionesdealmacenamiento;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ManejoArchivos {
    public static String LeeFlujo(FileInputStream archivo) {
        String regresa = "";
        StringBuffer sBuffer = new StringBuffer();
        InputStreamReader reader = new InputStreamReader(archivo);
        BufferedReader buffer = new BufferedReader(reader);
        try {
            regresa = buffer.readLine();
            while (regresa != null) {
                sBuffer.append(regresa);
                regresa = buffer.readLine();
            }
        } catch (IOException e) {
            Log.w("OpcionesAlmacenamiento", e.getMessage());
        }
        regresa = sBuffer.toString();
        return regresa;
    }
    public static void EscribeFlujo(FileOutputStream archivo, String datos) {
        OutputStreamWriter writer = new OutputStreamWriter(archivo);
        BufferedWriter buffer = new BufferedWriter(writer);
        try {
            buffer.write(datos);
            buffer.flush();
            buffer.close();
            writer.close();
        } catch (IOException e) {
            Log.w("OpcionesAlmacenamiento", e.getMessage());
        }
    }
    public static String LeeArchivo(File archivo) {
        try {
            FileInputStream flujo = new FileInputStream(archivo);
            return LeeFlujo(flujo);
        } catch (FileNotFoundException e) {
            Log.w("OpcionesAlmacenamiento", e.getMessage());
            return null;
        }
    }
    public static void EscribeArchivo(File archivo, String datos) {
        FileOutputStream flujo = null;
        try {
            flujo = new FileOutputStream(archivo);
        } catch (FileNotFoundException e) {
            Log.w("OpcionesAlmacenamiento", e.getMessage());
        }
        EscribeFlujo(flujo, datos);
    }
}
