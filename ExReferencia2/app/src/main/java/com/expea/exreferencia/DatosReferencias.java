package com.expea.exreferencia;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;

public class DatosReferencias {

	@SuppressLint("SimpleDateFormat") public void InsertarReferencia(Context ctx, String nombre, double lat, double lng)
	{		
		String DATE_FORMAT_NOW = "yyyy-MM-dd";
	    Date fecha = new Date();
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
	    String stringDate = sdf.format(fecha);
		
		ContentValues newValues = new ContentValues();
		//newValues.put("_id", id);
		newValues.put("NombreArchivo", nombre);		
		newValues.put("Latitud", lat);
		newValues.put("Longitud", lng);
		newValues.put("Fecha", stringDate);

		dbHelper.getHelper(ctx).Insertar("Referencias", newValues);

	}
	
	public void Subido(Context ctx, int valor)
	{
		ContentValues valores = new ContentValues();
		valores.put("Upload", true);
		
		dbHelper.getHelper(ctx).Actualizar("Referencias", valores, "_id", valor);
	}
	
	public void SubirArreglo(Context ctx, Referencia[] arreglo)
	{
		ContentValues valores = new ContentValues();
		valores.put("Upload", true);
		
		for(Referencia x : arreglo){
			dbHelper.getHelper(ctx).Actualizar("Referencias", valores, "_id", x.get_id());
		}
	}

}
