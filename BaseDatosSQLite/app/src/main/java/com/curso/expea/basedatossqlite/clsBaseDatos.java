package com.curso.expea.basedatossqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by marcos on 27/08/2015.
 */
public class clsBaseDatos extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "cont.db";
    public static final int DATABASE_VERSION = 1;

    private SQLiteDatabase myDataBase;

    public static final String SCRIPT_CREACION =
            "CREATE TABLE MisContactos " +
                    "(id integer primarykey autoincrement, " +
                    "NombreCompleto text, NumeroTelefonico text)";

    public clsBaseDatos(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLitedb) {
        sqLitedb.execSQL(SCRIPT_CREACION);
        myDataBase = sqLitedb;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLitedb, int i, int i1) {

    }

    public Cursor Consulta(String Cadena)
    {
        return myDataBase.rawQuery(Cadena,null);
    }

    public void Inserta(ContentValues valores)
    {
        myDataBase.insert("MisContactos", null, valores);
    }
}
