package EntidadAsesor;

public class colPedidosMostrador {
		private java.util.ArrayList<clsPedidosMostrador> fPedidosMos;

		public final java.util.ArrayList<clsPedidosMostrador> getLista() {
			return fPedidosMos;
		}

		public colPedidosMostrador() {
			fPedidosMos = new java.util.ArrayList<clsPedidosMostrador>();
		}

		public final java.util.ArrayList<String> ListaCadena() {
			java.util.ArrayList<String> Cadenas = new java.util.ArrayList<String>();
			for (int i = 0; i < fPedidosMos.size(); i++) {
				Cadenas.add(String.valueOf(fPedidosMos.get(i).getidPedidoCliente()));
			}
			return Cadenas;
		}

		public final java.util.Iterator<clsPedidosMostrador> GetEnumerator() {
			return (java.util.Iterator<clsPedidosMostrador>) fPedidosMos.iterator();
		}

		public final int getTotal() {
			return fPedidosMos.size();
		}

		public final clsPedidosMostrador Item(int Indice) {
			return fPedidosMos.get(Indice);
		}
	}
