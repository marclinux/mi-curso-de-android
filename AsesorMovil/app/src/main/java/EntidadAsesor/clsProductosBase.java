﻿package EntidadAsesor;

public class clsProductosBase {

	private int fidProducto;

	public final int getidProducto() {
		return fidProducto;
	}

	public final void setidProducto(int value) {
		fidProducto = value;
	}

	private String fClaveProducto;

	public final String getClaveProducto() {
		return fClaveProducto;
	}

	public final void setClaveProducto(String value) {
		fClaveProducto = value;
	}

	private String fDescripcionProd;

	public final String getDescripcionProd() {
		return fDescripcionProd;
	}

	public final void setDescripcionProd(String value) {
		fDescripcionProd = value;
	}

	private String fCodigoBarras;

	public final String getCodigoBarras() {
		return fCodigoBarras;
	}

	public final void setCodigoBarras(String value) {
		fCodigoBarras = value;
	}
}