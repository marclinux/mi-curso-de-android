package com.expea.curso.p04fragments;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentoUno extends Fragment {
    InterfaceMensaje mListener;
    Button btnAceptar;
    EditText edtNombre;
    public FragmentoUno() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragmento_uno, container, false);
        btnAceptar = view.findViewById(R.id.btnAceptar);
        edtNombre = view.findViewById(R.id.edtNombre);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = edtNombre.getText().toString();
                mListener.EnviaMensaje(mensaje);
            }
        });
        return view;
    }
    @Override
    public void onAttach(Activity actividad) {
        super.onAttach(actividad);
        try {
            mListener = (InterfaceMensaje) actividad;
        } catch (ClassCastException e) {
            throw new ClassCastException(actividad.toString() +
                    " debe implementar InterfaceMensaje");
        }
    }
}
