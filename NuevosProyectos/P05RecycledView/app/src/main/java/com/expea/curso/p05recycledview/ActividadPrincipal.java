package com.expea.curso.p05recycledview;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;


import com.expea.curso.p05recycledview.clases.clsUsuarios;

import java.util.ArrayList;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.selection.OnDragInitiatedListener;
import androidx.recyclerview.selection.OnItemActivatedListener;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ActividadPrincipal extends AppCompatActivity {
    RecyclerView VistaReciclada;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    //private RecycledView mRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        VistaReciclada = findViewById(R.id.my_recycler_view);
        VistaReciclada.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        VistaReciclada.setLayoutManager(mLayoutManager);

        clsUsuarios Usuario = new clsUsuarios();
        ArrayList<clsUsuarios> UsuariosLista = new ArrayList<clsUsuarios>();
        UsuariosLista = (ArrayList<clsUsuarios>) Usuario.getUsuarios();
        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(UsuariosLista);
        VistaReciclada.setAdapter(mAdapter);
        SelectionTracker selectionTracker = new SelectionTracker.Builder<>(
                "my-selection-id",
                VistaReciclada,
                new StableIdKeyProvider(VistaReciclada),
                new MyItemLookup(VistaReciclada),
                StorageStrategy.createLongStorage()
        )
                .withOnItemActivatedListener(new OnItemActivatedListener<Long>() {
                    @Override
                    public boolean onItemActivated(
                            @NonNull ItemDetailsLookup.ItemDetails<Long> item,
                            @NonNull MotionEvent e) {
                        Toast.makeText(getApplicationContext(),"Id Selecccionado: "
                                        + String.valueOf(item.getSelectionKey()),
                                Toast.LENGTH_LONG).show();
                        return true;
                    }
                })
                .withOnDragInitiatedListener(new OnDragInitiatedListener() {
                    @Override
                    public boolean onDragInitiated(@NonNull MotionEvent e) {
                        Toast.makeText(getApplicationContext(), "onDragInitiated",
                                Toast.LENGTH_LONG).show();
                        return true;
                    }

                })
                .build();
        mAdapter.setSelectionTracker(selectionTracker);
    }
}
