﻿package EntidadAsesor;

public class colProductosMovs {
	private java.util.ArrayList<clsProductosMovs> fProductosMovs;

	public final java.util.ArrayList<clsProductosMovs> getLista() {
		return fProductosMovs;
	}

	public colProductosMovs() {
		fProductosMovs = new java.util.ArrayList<clsProductosMovs>();
	}

	public final java.util.ArrayList<String> ListaCadena() {
		java.util.ArrayList<String> Cadenas = new java.util.ArrayList<String>();
		for (int i = 0; i < fProductosMovs.size(); i++) {
			Cadenas.add(fProductosMovs.get(i).getDescripcionProd());
		}
		return Cadenas;
	}

	public final java.util.Iterator<clsProductosMovs> GetEnumerator() {
		return (java.util.Iterator<clsProductosMovs>) fProductosMovs.iterator();
	}

	public final int getTotal() {
		return fProductosMovs.size();
	}

	public final clsProductosMovs Item(int Indice) {
		return fProductosMovs.get(Indice);
	}
}