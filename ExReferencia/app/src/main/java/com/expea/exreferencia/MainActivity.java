package com.expea.exreferencia;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private Button btnAceptar;
	private TextView txtUsuario;
	private TextView txtPassword;
	private Button btnRegistrarse;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnAceptar = (Button) findViewById(R.id.btnAceptar);
		txtUsuario = (TextView) findViewById(R.id.edtUsuario);
		txtPassword = (TextView) findViewById(R.id.edtPassword);
		btnRegistrarse = (Button) findViewById(R.id.btnRegistrarse);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		btnAceptar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				RevisaAcceso();
			}
		});
		
		btnRegistrarse.setOnClickListener(oclBtnRegistrarse);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	private void RevisaAcceso() {
		if (txtUsuario.getText().toString().trim().length() != 0 
			&& txtPassword.getText().toString().trim().length() != 0) {
			txtUsuario.setText("");
			txtPassword.setText("");
			Intent intent = new Intent(this, PrincipalActivity.class);
			startActivity(intent);					
		} else {
			Toast tx = Toast.makeText(getApplicationContext(),
					"Faltan valores", Toast.LENGTH_LONG);
			tx.show();
		}
	}
	
	OnClickListener oclBtnRegistrarse = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getBaseContext(), RegistroActivity.class);
			startActivity(intent);
		}
	};


}
