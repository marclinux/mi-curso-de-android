package com.example.multiplesactividades;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SitiosDao {
    @Query("SELECT * FROM Sitios ORDER BY NombreSitio")
    List<clsSitios> ObtenTodos();

    @Insert
    long insert(clsSitios sitio);

    @Delete
    void delete(clsSitios sitio);
}
