package com.curso.expea.basedatossqlite;

/**
 * Created by marcos on 27/08/2015.
 */
public class clsContactos {
    private int id;
    private String NombreCompleto;
    private String NumeroTelefonico;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getNumeroTelefonico() {
        return NumeroTelefonico;
    }

    public void setNumeroTelefonico(String numeroTelefonico) {
        NumeroTelefonico = numeroTelefonico;
    }
}
