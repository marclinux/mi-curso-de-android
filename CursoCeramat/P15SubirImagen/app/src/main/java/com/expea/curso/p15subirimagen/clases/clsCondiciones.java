package com.expea.curso.p15subirimagen.clases;

public class clsCondiciones {
    private int id;
    private String concepto;
    private int estatus;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getConcepto() {
        return concepto;
    }
    public void setConcepto(String cocepto) {
        this.concepto = cocepto;
    }
    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }
}
