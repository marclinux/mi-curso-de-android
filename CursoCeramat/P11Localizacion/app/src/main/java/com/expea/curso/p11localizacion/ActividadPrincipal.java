package com.expea.curso.p11localizacion;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvLatitud;
    TextView txvLongitud;
    Button btnObtener;
    LocationManager locManager;
    LocationListener listener;
    Location ubicacion = new Location("");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        iniciaVistas();
        iniciaLocationManager();
    }
    private void iniciaVistas() {
        txvLatitud = findViewById(R.id.txvLatitud);
        txvLongitud = findViewById(R.id.txvLongitud);
        btnObtener = findViewById(R.id.btnObtenerCoordenadas);
        btnObtener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txvLongitud.setText("Longitud: " +
                        String.valueOf(ubicacion.getLongitude()));
                txvLatitud.setText("LAtitud: " +
                        String.valueOf(ubicacion.getLatitude()));
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull
            String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (permissions.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        Criteria criterios = new Criteria();
                        criterios.setAccuracy(Criteria.ACCURACY_FINE);
                        criterios.setPowerRequirement(Criteria.POWER_MEDIUM);
                        criterios.setAltitudeRequired(false);
                        criterios.setSpeedRequired(false);
                        criterios.setCostAllowed(true);

                        String bestProvider = locManager.getBestProvider(criterios,
                                true);
                        locManager.requestLocationUpdates(bestProvider
                                /*LocationManager.GPS_PROVIDER*/,
                                0, 0, listener);
                    }
                }
                break;
        }
    }
    private void iniciaLocationManager() {
        locManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ubicacion = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Criteria criterios = new Criteria();
            criterios.setAccuracy(Criteria.ACCURACY_LOW);
            criterios.setPowerRequirement(Criteria.POWER_LOW);
            criterios.setAltitudeRequired(false);
            criterios.setSpeedRequired(false);
            criterios.setCostAllowed(false);

            String bestProvider = locManager.getBestProvider(criterios,
                    true);
            Toast.makeText(this, bestProvider, Toast.LENGTH_LONG).show();
            locManager.requestLocationUpdates(bestProvider
                    /*LocationManager.GPS_PROVIDER*/,
                    0, 0, listener);
        }
        else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }

    }
}
