package com.expea.curso.p04fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity
    implements InterfaceMensaje {
    Button btnAgregar;
    Button btnEliminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        btnAgregar = findViewById(R.id.btnAgregar);
        btnEliminar = findViewById(R.id.btnEliminar);
        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaccion =
                        getFragmentManager().beginTransaction();
                FragmentoUno fragmento1 = new FragmentoUno();
                transaccion.add(R.id.Fragmento1,(Fragment)fragmento1, "");
                transaccion.addToBackStack(null);
                transaccion.commit();
            }
        });
        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentoUno fragmento1 = (FragmentoUno)
                        getFragmentManager().findFragmentById(R.id.Fragmento1);
                if(fragmento1 != null) {
                    FragmentTransaction transaccion =
                            getFragmentManager().beginTransaction();
                    transaccion.remove(fragmento1);
                    transaccion.commit();
                }
            }
        });
    }

    @Override
    public void EnviaMensaje(String mensaje) {
        FragmentoDos fragmento2 = (FragmentoDos)
                getFragmentManager().findFragmentById(
                        R.id.Fragmento2
                );
        fragmento2.RecibeDatos(mensaje);
    }
}
