package com.expea.curso.p13volley.clases;

public class clsUsuarios {
    private String cliente;
    private  String nombre;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
