package com.example.ariel.basededatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ariel on 27/08/2015.
 */
public class clsSqLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="miscontactos.db";
    private static final int DATABASE_VERSION=2;
      // Contacts table name
    private static final String TABLE_NAME = "miscontacts";
    // Contacts Table Columns names
    private static final String ID = "id";
    private static final String NOMBRE = "nombre";
    private static final String TEL = "telefono";

    public clsSqLiteHelper(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.w("si", " \n\n\n\n\ncreada\n\n\n\n\n");
        String sqlTablaContactos = "CREATE TABLE " + TABLE_NAME + " ("
                + ID + " INTEGER PRIMARY KEY autoincrement," + NOMBRE + " TEXT,"
                + TEL + " TEXT" + ")";
        db.execSQL(sqlTablaContactos);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }


        // Adding new contact
        public void addContact(Contacto contact) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(NOMBRE, contact.getNombre()); // Contact Name
            values.put(TEL, contact.getTel()); // Contact Phone Number

            // Inserting Row
            db.insert(TABLE_NAME, null, values);
            db.close(); // Closing database connection
        }

        // Getting single contact
        public Contacto getContact(int id) {
            SQLiteDatabase db = this.getReadableDatabase();
            Contacto contact=null;
            String sql="select *  from " + TABLE_NAME + " where id=" + id;
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                contact = new Contacto(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2));
            }

            db.close();

            // return contact
            return contact;
        }

        // Getting All Contacts
        public List<Contacto> getAllContacts() {
            List<Contacto> contactList = new ArrayList<Contacto>();
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_NAME;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Contacto contacto = new Contacto(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2) );

                    // Adding contact to list
                    contactList.add(contacto);
                } while (cursor.moveToNext());
            }

            // return contact list
            return contactList;
        }

        // Getting contacts Count
        public int getContactsCount() {
            String countQuery = "SELECT  * FROM " + TABLE_NAME;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            cursor.close();

            // return count
            return cursor.getCount();
        }
        // Updating single contact
        public int updateContact(Contacto contact) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(NOMBRE, contact.getNombre());
            values.put(TEL, contact.getId());

            // updating row
            return db.update(TABLE_NAME, values, ID + " = ?",
                    new String[] { String.valueOf(contact.getId()) });
        }

        // Deleting single contact
        public void deleteContact(Contacto contact) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_NAME, ID + " = ?",
                    new String[] { String.valueOf(contact.getId()) });
            db.close();
        }
}
