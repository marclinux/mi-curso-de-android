package com.expea.curso.p11locationmanager;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadPrincipal extends AppCompatActivity {
    LocationManager locManager;
    Location ultimaUbicacion = new Location("");
    TextView txvLatitud;
    TextView txvLongitud;
    Button btnObtenerCoordenadas;
    LocationListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        IniciaLocationManager();
        IniciaVistas();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Criteria criterios = new Criteria();
                    criterios.setAccuracy(Criteria.ACCURACY_FINE);
                    criterios.setPowerRequirement(Criteria.POWER_MEDIUM);
                    criterios.setAltitudeRequired(false);
                    criterios.setSpeedRequired(false);
                    criterios.setCostAllowed(true);

                    String bestProvider = locManager.getBestProvider(criterios, true);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)
                        locManager.requestLocationUpdates(bestProvider /*LocationManager.GPS_PROVIDER*/, 0, 0, listener);

                } else {
                    Toast.makeText(getApplicationContext(), "No se acepto el permiso",
                            Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void IniciaLocationManager() {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ultimaUbicacion.set(location);
                Log.w("MiApplicacion", String.valueOf(location.getLongitude()));
                Log.w("MiApplicacion", String.valueOf(location.getLatitude()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            //return;
        }
        else
        {
            Criteria criterios = new Criteria();
            criterios.setAccuracy(Criteria.ACCURACY_FINE);
            criterios.setPowerRequirement(Criteria.POWER_MEDIUM);
            criterios.setAltitudeRequired(false);
            criterios.setSpeedRequired(false);
            criterios.setCostAllowed(true);

            String bestProvider = locManager.getBestProvider(criterios, true);
            locManager.requestLocationUpdates(bestProvider /*LocationManager.GPS_PROVIDER*/, 0, 0, listener);
        }
    }

    private void IniciaVistas() {
        txvLatitud = findViewById(R.id.txvLatitud);
        txvLongitud = findViewById(R.id.txvLongitud);
        btnObtenerCoordenadas = findViewById(R.id.btnObtenerCoordenadas);
        btnObtenerCoordenadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ultimaUbicacion != null) {
                    txvLatitud.setText("Latitud: " + String.valueOf(ultimaUbicacion.getLatitude()));
                    txvLongitud.setText("Longitud: " + String.valueOf(ultimaUbicacion.getLongitude()));
                }
            }
        });
    }
}
