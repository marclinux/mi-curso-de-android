package com.expea.curso.p03layoutsavanzados;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.expea.curso.p03layoutsavanzados.Clases.clsUsuarios;

public class ActividadPrincipal extends AppCompatActivity {

    public clsUsuarios UsuarioSesion;
    EditText Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        Login = (EditText) findViewById(R.id.edtLogin);
        UsuarioSesion = new clsUsuarios();
    }

    public void btnAceptarOnClick(View view) {
        UsuarioSesion.setId(1);
        UsuarioSesion.setLogin(Login.getText().toString());
        UsuarioSesion.setNivel(99);
        Intent intent = new Intent(getApplicationContext(), ActividadMenu.class);
        intent.putExtra("usuarioSesion", UsuarioSesion);
        startActivity(intent);
    }
}
