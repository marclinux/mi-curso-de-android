package com.expea.exreferencia;

import android.content.Context;

/**
 * Created by marcos on 28/09/2015.
 */
public class dbHelper extends FabricaDatos {
    private static dbHelper instance;

    public static synchronized dbHelper getHelper(Context context)
    {
        if (instance == null)
            instance = new dbHelper(context);

        return instance;
    }

    public dbHelper(Context ctx) {
        super(ctx);
    }
}
