package com.curso.expea.fragmentos;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    Button btnOpcion1;

    Button btnOpcion2;

    Fragment fragmento;

    FragmentTransaction fragTransaction;

    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_menu, container, false);

        btnOpcion1 = (Button) v.findViewById(R.id.btnOpcion1);
        btnOpcion2 = (Button) v.findViewById(R.id.btnOpcion2);
//
//        fragmento = new OPcion1Fragment();
//
//        fragTransaction = getFragmentManager().beginTransaction().add(R.id.frgOpcion1, fragmento);
//        fragTransaction.commit();
//
        btnOpcion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmento = new OPcion1Fragment();

                fragTransaction = getFragmentManager().beginTransaction().replace(R.id.frgOpcion1, fragmento);
                fragTransaction.commit();
            }
        });

        btnOpcion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmento = new Opcion2Fragment();

                fragTransaction = getFragmentManager().beginTransaction().replace(R.id.frgOpcion1, fragmento);
                fragTransaction.commit();
            }
        });
        return v;
    }


}
