package com.expea.curso.p05recycledview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.expea.curso.p05recycledview.clases.clsUsuarios;

import java.util.ArrayList;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<clsUsuarios> mDataset;
    private SelectionTracker selectionTracker;

    public SelectionTracker getSelectionTracker() {
        return selectionTracker;
    }

    public void setSelectionTracker(SelectionTracker selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txvId;
        private TextView txvLogin;
        private TextView txvNivel;
        public MyViewHolder(View v) {
            super(v);
            setTxvId((TextView)v.findViewById(R.id.txvId));
            setTxvLogin((TextView)v.findViewById(R.id.txvLogin));
            setTxvNivel((TextView)v.findViewById(R.id.txvNivel));
        }
        public TextView getTxvId() {
            return txvId;
        }
        public void setTxvId(TextView txvId) {
            this.txvId = txvId;
        }
        public TextView getTxvLogin() {
            return txvLogin;
        }
        public void setTxvLogin(TextView txvLogin) {
            this.txvLogin = txvLogin;
        }
        public TextView getTxvNivel() {
            return txvNivel;
        }
        public void setTxvNivel(TextView txvNivel) {
            this.txvNivel = txvNivel;
        }

        public ItemDetailsLookup.ItemDetails getItemDetails() {
            return new MyItemDetail(getAdapterPosition(), mDataset.get(getAdapterPosition()));
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<clsUsuarios> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lista, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.getTxvId().setText(String.valueOf(mDataset.get(position).getId()));
        holder.getTxvLogin().setText(mDataset.get(position).getLogin());
        holder.getTxvNivel().setText(String.valueOf(mDataset.get(position).getNivel()));
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}