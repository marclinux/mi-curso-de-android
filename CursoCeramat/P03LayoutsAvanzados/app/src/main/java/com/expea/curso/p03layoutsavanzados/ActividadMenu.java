package com.expea.curso.p03layoutsavanzados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.expea.curso.p03layoutsavanzados.Clases.clsUsuarios;

public class ActividadMenu extends AppCompatActivity {

    private clsUsuarios UsuarioSesion;
    TextView txvLogin;
    TextView txvNivel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_menu);
        txvLogin = findViewById(R.id.txvLogin);
        txvNivel = findViewById(R.id.txvNivel);
        UsuarioSesion = new clsUsuarios();
        Bundle data = getIntent().getExtras();
        UsuarioSesion = (clsUsuarios) data.getParcelable("usuarioSesion");
        txvLogin.setText(UsuarioSesion.getLogin());
        txvNivel.setText(String.valueOf(UsuarioSesion.getNivel()));
    }
}
