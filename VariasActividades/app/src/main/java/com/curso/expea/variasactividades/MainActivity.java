package com.curso.expea.variasactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtMensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtMensaje = (EditText) findViewById(R.id.edtMensaje);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void OnClick_Opcion1(View vista)
    {
        //Toast.makeText(MainActivity.this, "Se eligió la primera opción", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getBaseContext(), Opcion1Activity.class);
        String cadena = edtMensaje.getText().toString();
        intent.putExtra("Mensaje", cadena);
        startActivity(intent);
    }

    public void OnClick_Opcion2(View vista)
    {
        Toast.makeText(MainActivity.this, "Se eligió la segunda opción", Toast.LENGTH_SHORT).show();
    }
}
