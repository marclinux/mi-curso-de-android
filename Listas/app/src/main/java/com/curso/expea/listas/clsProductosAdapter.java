package com.curso.expea.listas;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by marcos on 24/08/2015.
 */
public class clsProductosAdapter extends ArrayAdapter <clsProductosPrecios> {

    private int layoutResourceId;

    public clsProductosAdapter(Context context, int resource, List<clsProductosPrecios> items) {
        super(context, resource, items);
        layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(this.layoutResourceId, parent, false);
        }

        clsProductosPrecios p = new clsProductosPrecios();
        p = getItem(position);
        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.txvClave);
            TextView tt2 = (TextView) v.findViewById(R.id.txvDescripcion);
            TextView tt3 = (TextView) v.findViewById(R.id.txvPrecio);
            if (tt1 != null) {
                tt1.setText(p.getClaveProducto());
            }
            if (tt2 != null) {
                tt2.setText(p.getDescripcionProd());
            }
            if (tt3 != null) {
                tt3.setText(String.valueOf(p.getPrecioUnit()));
            }
        }
        if(position % 2 == 0)
            v.setBackgroundColor(Color.BLACK);
        else
            v.setBackgroundColor(Color.BLUE);
        return v;
    }
}
