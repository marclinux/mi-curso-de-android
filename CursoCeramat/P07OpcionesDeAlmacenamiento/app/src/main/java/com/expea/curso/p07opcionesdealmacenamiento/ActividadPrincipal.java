package com.expea.curso.p07opcionesdealmacenamiento;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class ActividadPrincipal extends AppCompatActivity {
    EditText edvValor;
    Button btnLeerShared;
    Button btnEscribirShared;
    Button btnLeerCache;
    Button btnEscribirCache;
    Button btnLeerExterno;
    Button btnEscribirExterno;
    Button btnLeerInterno;
    Button btnEscribirInterno;
    int valor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        edvValor = findViewById(R.id.edtValor);
        btnLeerShared = findViewById(R.id.btnLeershared);
        btnEscribirShared = findViewById(R.id.btnEscribirShared);
        btnLeerCache = findViewById(R.id.btnleerCache);
        btnEscribirCache = findViewById(R.id.btnEscribirCache);
        btnLeerExterno = findViewById(R.id.btnLeerExterno);
        btnEscribirExterno = findViewById(R.id.btnEscribirExterno);
        btnLeerInterno = findViewById(R.id.btnLeerInterno);
        btnEscribirInterno = findViewById(R.id.btnEscribirInterno);
        btnLeerShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences misPreferencias = getSharedPreferences(
                        "misPreferencias",0
                );
                valor = misPreferencias.getInt("valor", 0);
                edvValor.setText(String.valueOf(valor));
            }
        });
        btnEscribirShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valor = Integer.valueOf(edvValor.getText().toString());
                SharedPreferences misPreferencias = getSharedPreferences(
                        "misPreferencias",0
                );
                SharedPreferences.Editor editor = misPreferencias.edit();
                editor.putInt("valor", valor);
                editor.commit();
            }
        });
        btnLeerCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File archivo = new File(getCacheDir(), "MiCache");
                String cadena = ManejoArchivos.LeeArchivo(archivo);
                edvValor.setText(cadena);
            }
        });
        btnEscribirCache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File archivo = new File(getCacheDir(), "MiCache");
                String cadena = edvValor.getText().toString();
                ManejoArchivos.EscribeArchivo(archivo, cadena);
            }
        });
        btnLeerExterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File archivo = new File(getExternalFilesDir(DIRECTORY_DOWNLOADS),
                        "MiArchivoExterno.txt");
                String cadena = ManejoArchivos.LeeArchivo(archivo);
                edvValor.setText(cadena);
            }
        });
        btnEscribirExterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File archivo = new File(getExternalFilesDir(DIRECTORY_DOWNLOADS),
                                        "MiArchivoExterno.txt");
                String cadena = edvValor.getText().toString();
                ManejoArchivos.EscribeArchivo(archivo, cadena);
            }
        });
        btnLeerInterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileInputStream flujo = null;
                try {
                    flujo = openFileInput("valores.txt");
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(),
                            Toast.LENGTH_LONG);
                }
                String cadena = ManejoArchivos.LeeFlujo(flujo);
                edvValor.setText(cadena);
            }
        });
        btnEscribirInterno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileOutputStream flujo = null;
                try {
                    flujo = openFileOutput("valores.txt",
                            Context.MODE_PRIVATE);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(),
                            Toast.LENGTH_LONG);
                }
                String cadena = edvValor.getText().toString();
                ManejoArchivos.EscribeFlujo(flujo, cadena);
            }
        });
    }
}
