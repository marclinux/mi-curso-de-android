package com.curso.expea.listas;

/**
 * Created by marcos on 24/08/2015.
 */
public class clsProductosPrecios {
    private int fidProducto;

    public final int getidProducto() {
        return fidProducto;
    }

    public final void setidProducto(int value) {
        fidProducto = value;
    }

    private String fClaveProducto;

    public final String getClaveProducto() {
        return fClaveProducto;
    }

    public final void setClaveProducto(String value) {
        fClaveProducto = value;
    }

    private String fDescripcionProd;

    public final String getDescripcionProd() {
        return fDescripcionProd;
    }

    public final void setDescripcionProd(String value) {
        fDescripcionProd = value;
    }

    private float fPrecioUnit;

    public final float getPrecioUnit() {
        return fPrecioUnit;
    }

    public final void setPrecioUnit(float value) {
        fPrecioUnit = value;
    }
}
