﻿package EntidadAsesor;

public class clsProductosPrecios extends clsProductosBase {
	private float fPrecioUnit;

	public final float getPrecioUnit() {
		return fPrecioUnit;
	}

	public final void setPrecioUnit(float value) {
		fPrecioUnit = value;
	}

	private float fCostoPromedio;

	public final float getCostoPromedio() {
		return fCostoPromedio;
	}

	public final void setCostoPromedio(float value) {
		fCostoPromedio = value;
	}

	private float fIva;

	public final float getIva() {
		return fIva;
	}

	public final void setIva(float value) {
		fIva = value;
	}

	private int fExistenciaProd;

	public final int getExistenciaProd() {
		return fExistenciaProd;
	}

	public final void setExistenciaProd(int value) {
		fExistenciaProd = value;
	}
}