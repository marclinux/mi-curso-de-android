﻿package EntidadAsesor;

public class clsPedidosMostrador extends clsPedidosClientes {

	public clsPedidosMostrador() {
		super();
		fPersonas = new colPersonasPedido();
	}

	private colPersonasPedido fPersonas;

	public final colPersonasPedido getPersonas() {
		return fPersonas;
	}

	public final void setPersonas(colPersonasPedido value) {
		fPersonas = value;
	}

	private int fidCorte;

	public final int getidCorte() {
		return fidCorte;
	}

	public final void setidCorte(int value) {
		fidCorte = value;
	}

	private boolean fDistribucionesIguales;

	public final boolean getDistribucionesIguales() {
		return fDistribucionesIguales;
	}

	public final void setDistribucionesIguales(boolean value) {
		fDistribucionesIguales = value;
	}

	private int fCerrado;

	public final int getCerrado() {
		return fCerrado;
	}

	public final void setCerrado(int value) {
		fCerrado = value;
	}

	private int fTipoVenta;

	public final int getTipoVenta() {
		return fTipoVenta;
	}

	public final void setTipoVenta(int value) {
		fTipoVenta = value;
	}

}