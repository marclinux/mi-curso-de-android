package com.example.ariel.basededatos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView lsvContactos;
    ArrayList<Contacto> contactos;

    private clsSqLiteHelper bd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bd=new clsSqLiteHelper(getApplicationContext());
        bd.addContact(new Contacto(5, "dato nuevo", "123456"));
        //Log.w("", "base de datos CREADA");
        //Log.w("dededededededededede", bd.getContact(1).getNombre());

        lsvContactos=(ListView)findViewById(R.id.lsvContactos);
        contactos=new ArrayList<Contacto>();
        contactos.add(new Contacto(1,bd.getContact(1).getNombre(),"ded"));
        llenaContactos();
        ContactosAdapter p=new ContactosAdapter(getApplicationContext(),R.layout.item_layout,contactos);
        lsvContactos.setAdapter(p);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private  void llenaContactos(){
        /*contactos.add(new Contacto(1,"ariel lópez","123456"));
        contactos.add(new Contacto(1,"angel lópez","123457"));
        contactos.add(new Contacto(1,"eduardo lópez","123458"));*/
        contactos= (ArrayList<Contacto>) bd.getAllContacts();
    }
}
