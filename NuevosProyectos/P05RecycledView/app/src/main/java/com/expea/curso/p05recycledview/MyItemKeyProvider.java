package com.expea.curso.p05recycledview;

import android.content.ClipData;

import com.expea.curso.p05recycledview.clases.clsUsuarios;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

public class MyItemKeyProvider extends ItemKeyProvider {
    private final List<clsUsuarios> itemList;

    public MyItemKeyProvider(int scope, List<clsUsuarios> itemList) {
        super(scope);
        this.itemList = itemList;
    }

    @Nullable
    @Override
    public Object getKey(int position) {
        return itemList.get(position);
    }

    @Override
    public int getPosition(@NonNull Object key) {
        return itemList.indexOf(key);
    }
}
