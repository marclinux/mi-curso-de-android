package com.ati.asesormovil;

import java.sql.Date;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.Request;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import EntidadAsesor.*;

public class PedidosActivity extends ActionBarActivity {

	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	private ArrayList<clsPedidosMostrador> pedidos;
	private TextView txvNombreUsu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pedidos);

		String Nivel = "";
		if(Global.UsuarioSesion.getNivel() > 20)
			Nivel = "Supervisor";
		else
			Nivel = "Asesor";
		pedidos = new ArrayList<clsPedidosMostrador>();

		txvNombreUsu = (TextView) findViewById(R.id.txvNombreUsu);
		txvNombreUsu.setText("Usuario: " + Global.UsuarioSesion.getNombreCompleto() +
							" Nivel: " + Nivel);
		
		gridView = (GridView) findViewById(R.id.grdPedidos);
		customGridAdapter = new GridViewAdapter(this, R.layout.celda_grid,
				pedidos);
		gridView.setAdapter(customGridAdapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Global.Pedido = pedidos.get(position);
				/*
				 * Toast tx = Toast.makeText(getApplicationContext(),
				 * String.valueOf(Global.Pedido.getidPedidoCliente()),
				 * Toast.LENGTH_SHORT); tx.show();
				 */
				Global.Nuevo = false;
				AgregarPedido();
			}
		});

		ObtPedidos();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pedidos, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.Agregar:
			Global.Nuevo = true;
			AgregarPedido();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		ObtPedidos();
	}

	public void AgregarPedido() {
		Intent intent = new Intent(this, PedidoActivity.class);
		startActivity(intent);
	}

	public void ObtPedidos() {
		String url = "http://"+ Global.Servidor + 
					"/WebServAsesor/index.php/rest/pedidosmostrador/" +
					String.valueOf(Global.UsuarioSesion.getIdUsuario());
		JsonObjectRequest jr = new JsonObjectRequest(Request.Method.GET, url,
				null, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						parseJSON(response);
						customGridAdapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {
				    @Override
				    public void onErrorResponse(VolleyError error) {
				        //VolleyLog.e("Error: ", error.getMessage());
				    	Toast tx = Toast.makeText(getApplicationContext(),
								"Ocurrio el siguiente error: " +
								error.getMessage(), Toast.LENGTH_LONG);
						tx.show();
				    }
				});
		Global.mRequestQueue.add(jr);
	}

	private void parseJSON(JSONObject json) {
		try {
			JSONArray items = json.getJSONArray("items");
			pedidos.clear();
			for (int i = 0; i < items.length(); i++) {
				JSONObject item = items.getJSONObject(i);
				JSONObject cliente = item.getJSONObject("idCliente0");
				clsPedidosMostrador nm = new clsPedidosMostrador();
				nm.setidPedidoCliente(item.optInt("idPedidoMostrador"));
				nm.setidUsuario(item.optInt("idUsuario"));
				nm.getCliente().setidCliente(item.optInt("idCliente"));
				String paramDateAsString = item.optString("FechaPedido")
						.substring(0, 10);
				nm.getCliente().setApellidosCliente(
						cliente.optString("ApellidosCliente"));
				nm.getCliente().setNombresCliente(
						cliente.optString("NombresCliente"));
				nm.setFechaPedido(Date.valueOf(paramDateAsString));
				pedidos.add(nm);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
