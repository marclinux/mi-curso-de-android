package com.curso.expea.listaencurso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lsvProductos;

    ArrayList<clsProductos> productos;

    clsProductos prod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lsvProductos = (ListView) findViewById(R.id.lsvProductos);

        productos = new ArrayList<clsProductos>();
        LlenaProductos();

        ProductosAdapter adaptador = new ProductosAdapter(
                getApplicationContext(),
                R.layout.item_layout,
                productos);

        lsvProductos.setAdapter(adaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void LlenaProductos()
    {
        prod = new clsProductos();
        prod.setClave("P0002");
        prod.setDescripcion("COCA COLA 600 ML");
        prod.setPrecio(12.5f);
        productos.add((prod));

        prod = new clsProductos();
        prod.setClave("P0003");
        prod.setDescripcion("FANTA 600 ML");
        prod.setPrecio(10.5f);
        productos.add((prod));

        prod = new clsProductos();
        prod.setClave("P0004");
        prod.setDescripcion("MANZANITA SOL 600 ML");
        prod.setPrecio(10.5f);
        productos.add((prod));


    }
}
