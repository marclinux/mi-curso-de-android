package com.curso.expea.listaencurso;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by marcos on 26/08/2015.
 */
public class ProductosAdapter extends ArrayAdapter<clsProductos> {

    private int idResource;
    public ProductosAdapter(Context context, int resource, ArrayList<clsProductos> objects) {
        super(context, resource, objects);
        idResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null)
        {
            LayoutInflater linflater;
            linflater = LayoutInflater.from(getContext());
            v = linflater.inflate(idResource, parent, false);
        }

        clsProductos p;

        p = getItem(position);

        TextView txvClave = (TextView) v.findViewById(R.id.txvClave);
        TextView txvDescripcion = (TextView) v.findViewById(R.id.txvDescripcion);
        TextView txvPrecio = (TextView) v.findViewById(R.id.txvPrecio);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.txvClave);
            TextView tt2 = (TextView) v.findViewById(R.id.txvDescripcion);
            TextView tt3 = (TextView) v.findViewById(R.id.txvPrecio);
            if (tt1 != null) {
                tt1.setText(p.getClave());
            }
            if (tt2 != null) {
                tt2.setText(p.getDescripcion());
            }
            if (tt3 != null) {
                tt3.setText(String.valueOf(p.getPrecio()));
            }
        }
        if(position % 2 == 0)
            v.setBackgroundColor(Color.BLACK);
        else
            v.setBackgroundColor(Color.BLUE);

        return v;
    }
}
