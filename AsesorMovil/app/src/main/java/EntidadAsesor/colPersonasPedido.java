﻿package EntidadAsesor;

public class colPersonasPedido implements Iterable<clsPersonasPedido> {
	private java.util.ArrayList<clsPersonasPedido> fPersonasPedido;

	public final java.util.ArrayList<clsPersonasPedido> getLista() {
		return fPersonasPedido;
	}

	public colPersonasPedido() {
		fPersonasPedido = new java.util.ArrayList<clsPersonasPedido>();
	}

	public final java.util.ArrayList<String> ListaCadena() {
		java.util.ArrayList<String> Cadenas = new java.util.ArrayList<String>();
		for (int i = 0; i < fPersonasPedido.size(); i++) {
			Cadenas.add(fPersonasPedido.get(i).getNombreCompleto());
		}
		return Cadenas;
	}

	public final java.util.Iterator<clsPersonasPedido> iterator() {
		return (java.util.Iterator<clsPersonasPedido>) fPersonasPedido
				.iterator();
	}

	public final int getTotal() {
		return fPersonasPedido.size();
	}

	public final void Limpiar() {
		fPersonasPedido.clear();
	}

	public final clsPersonasPedido Item(int Indice) {
		return fPersonasPedido.get(Indice);
	}

	public final void Agregar(clsPersonasPedido Producto) {
		fPersonasPedido.add(Producto);
	}

	public final void Eliminar(int Indice) {
		fPersonasPedido.remove(Indice);
	}

	public final float ObtenerTotal() {
		float rTotal = 0;
		for (int i = 0; i < fPersonasPedido.size(); i++) {
			rTotal += fPersonasPedido.get(i).getMontoPagar();
		}
		return rTotal;
	}
}