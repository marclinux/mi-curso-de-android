package com.ati.asesormovil;

import java.util.ArrayList;

import EntidadAsesor.clsPedidosMostrador;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * 
 * @author
 * 
 */
public class GridViewAdapter extends ArrayAdapter {
	private Context context;
	private int layoutResourceId;
	private ArrayList<clsPedidosMostrador> data = new ArrayList<clsPedidosMostrador>();

	public GridViewAdapter(Context context, int layoutResourceId,
			ArrayList<clsPedidosMostrador> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ViewHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.Titulo = (TextView) row.findViewById(R.id.txtIdPedido);
			holder.Subtitulo = (TextView) row
					.findViewById(R.id.txtNombreCliente);
			holder.Fecha = (TextView) row.findViewById(R.id.txtFecha);
			row.setTag(holder);
		} else {
			holder = (ViewHolder) row.getTag();
		}
		clsPedidosMostrador item = new clsPedidosMostrador();
		item = data.get(position);
		holder.Titulo.setText("Pedido: " + String.valueOf(item.getidPedidoCliente()));
		if(item.getCliente().getidCliente() == 0)
			holder.Subtitulo.setText("Cliente de mostrador");
		else
			holder.Subtitulo.setText(String.valueOf(item.getCliente().getApellidosCliente().concat(" ").concat(
													item.getCliente().getNombresCliente())));
		holder.Fecha.setText(String.valueOf(item.getFechaPedido()));
		if(item.getidUsuario() == Global.UsuarioSesion.getIdUsuario())
			row.setBackgroundColor(0x30FF0000);
		else
			row.setBackgroundColor( 0x300000FF);
		return row;
	}

	static class ViewHolder {
		TextView Titulo;
		TextView Subtitulo;
		TextView Fecha;
	}
}
