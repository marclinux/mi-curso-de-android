package com.expea.exreferencia;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PrincipalActivity extends ActionBarActivity {

	Button btnTomarFoto;
	Button btnConsultar;
	Button btnConfiguracion;
	Button btnBajarDatos;
	Button btnSalir;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);

		btnTomarFoto = (Button) findViewById(R.id.btnTomarFoto);
		btnConsultar = (Button) findViewById(R.id.btnConsultar);
		btnConfiguracion = (Button) findViewById(R.id.btnConfiguracion);
		btnBajarDatos = (Button) findViewById(R.id.btnBajarDatos);
		btnSalir = (Button) findViewById(R.id.btnSalir);

		btnTomarFoto.setOnClickListener(oclBtnTomarFoto);
		btnConsultar.setOnClickListener(oclBtnConsultar);
		btnConfiguracion.setOnClickListener(oclBtnConfigurar);
		btnBajarDatos.setOnClickListener(oclBtnBajarDatos);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;

	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type){
	      return Uri.fromFile(getOutputMediaFile(type));
	}
	
	private Uri fileUri;
	private static File getOutputMediaFile(int type){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp", "failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMG_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "VID_"+ timeStamp + ".mp4");
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
	
	OnClickListener oclBtnTomarFoto = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		    //fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE); // create a file to save the image
		    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

		    // start the image capture Intent
		    startActivityForResult(intent, 100);
		}
	};

	OnClickListener oclBtnConfigurar = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// change text of the TextView (tvOut)
			Intent intent2 = new Intent(getBaseContext(),
					ConfiguracionActivity.class);
			startActivity(intent2);
		}
	};

	OnClickListener oclBtnBajarDatos = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// change text of the TextView (tvOut)
			Intent intent2 = new Intent(getBaseContext(),
					BajarDActivity.class);
			startActivity(intent2);
		}
	};

	OnClickListener oclBtnConsultar = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// change text of the TextView (tvOut)
			Intent intent2 = new Intent(getBaseContext(),
					ConsultaActivity.class);
			startActivity(intent2);
		}
	};
}
