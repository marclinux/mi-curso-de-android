package com.expea.curso.p15subirimagen;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.expea.curso.p15subirimagen.clases.clsCondiciones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class ActividadPrincipal extends AppCompatActivity {
    EditText edtDato1;
    Spinner spnDato2;
    Button btnObtenerImagen;
    Button btnEnviar;
    String url = "http://10.1.52.233:8080/apiinventario/getconceptos.php?opcion=condicion";
    RequestQueue queue;
    ArrayList<clsCondiciones> Condiciones;
    ArrayList<String> lstCondiciones = new ArrayList<String>();
    Bitmap selectedImage;
    ImageView imvImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        queue = Volley.newRequestQueue(getApplicationContext());
        Condiciones = new ArrayList<clsCondiciones>();
        iniciaVistas();
    }

    private void iniciaVistas() {
        edtDato1 = findViewById(R.id.edtDato1);
        spnDato2 = findViewById(R.id.spnDato2);
        btnObtenerImagen = findViewById(R.id.btnObtenerImagen);
        btnEnviar = findViewById(R.id.btnEnviar);
        imvImagen = findViewById(R.id.imvImagen);
        btnObtenerImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromAlbum();
            }
        });
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        StringRequest request = new StringRequest(StringRequest.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ParseCondicones(response);
                lstCondiciones.clear();
                for (int i = 0; i < Condiciones.size(); i++) {
                    lstCondiciones.add(Condiciones.get(i).getConcepto());
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        ActividadPrincipal.this,
                        android.R.layout.simple_spinner_item, lstCondiciones);
                spnDato2.setAdapter(arrayAdapter);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
        queue.add(request);
    }

    private void ParseCondicones(String response) {
        JSONObject consulta = null;
        try {
            consulta = new JSONObject(response);
            if (consulta != null) {
                boolean estatus = consulta.getBoolean("estatus");
                if (estatus) {
                    JSONObject jResultado = consulta.getJSONObject("resultados");
                    JSONArray jCondiciones = jResultado.getJSONArray("condicion");
                    int total = jCondiciones.length();
                    Condiciones.clear();
                    for (int i = 0; i < total; i++) {
                        JSONObject jCondicion = jCondiciones.getJSONObject(i);
                        clsCondiciones lCondicion = new clsCondiciones();
                        lCondicion.setId(jCondicion.getInt("id"));
                        lCondicion.setConcepto(jCondicion.getString("concepto"));
                        lCondicion.setEstatus(jCondicion.getInt("estatus"));
                        Condiciones.add(lCondicion);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getImageFromAlbum() {
        try {
            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 1);
        } catch (Exception exp) {
            Toast.makeText(ActividadPrincipal.this,
                    exp.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver()
                        .openInputStream(imageUri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
                imvImagen.setImageBitmap(selectedImage);
            } catch (FileNotFoundException e) {
                Toast.makeText(ActividadPrincipal.this, e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    public class OkHttpAsync extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... strings) {
            String mensaje = "";
            OkHttpClient httpClient = new OkHttpClient();
            //creates multipart http requests
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("purpose", "xyz store coupons")
                    .addFormDataPart("xml", "xyz_store_coupons.xml",
                            RequestBody.create(MediaType.parse("application/xml"),
                                    new File("website/static/xyz_store_coupons.xml")))
                    .addFormDataPart("image", "foto1.jpg",
                            RequestBody.create("", new File()))
                    .build();

            Request request = new Request.Builder()
                    .url("https://dummy....")
                    .post(requestBody)
                    .build();
            okhttp3.Response response = null;
            try {
                response = httpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    mensaje = "Got response from server for multipart request using OkHttp ";
                }

            } catch (IOException e) {
                mensaje= "error in getting response for multipart request okhttp";
            }
            return mensaje;
        }
    }
}
