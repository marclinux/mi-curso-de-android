package com.expea.curso.p13volley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.expea.curso.p13volley.clases.clsUsuarios;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvResultado;
    Button btnIniciar;
    ListView lstUsuarios;
    RequestQueue queue;
    String url = "http://10.1.52.233:8080/apiventas/getclientes.php?agente=000003";
    ArrayList<clsUsuarios> Usuarios = new ArrayList<clsUsuarios>();
    UsuariosAdapter usuariosAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        queue = Volley.newRequestQueue(getApplicationContext());
        iniciaVistas();
    }

    private void iniciaVistas() {
        txvResultado = findViewById(R.id.txvRespuesta);
        btnIniciar = findViewById(R.id.btnIniciar);
        lstUsuarios = findViewById(R.id.lstUsuarios);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest request = new StringRequest(StringRequest.Method.GET,
                        url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject consulta = null;
                        try {
                            consulta = new JSONObject(response);
                            if (consulta != null) {
                                txvResultado.setText("Estatus : " +
                                        consulta.getString("estatus"));
                                JSONArray jResultado = consulta.getJSONArray("resultados");
                                int total = jResultado.length();
                                Usuarios.clear();
                                for (int i = 0; i < total; i++) {
                                    JSONObject jUsuario = jResultado.getJSONObject(i);
                                    clsUsuarios lUsuario = new clsUsuarios();
                                    lUsuario.setCliente(jUsuario.getString("cliente"));
                                    lUsuario.setNombre(jUsuario.getString("nombre"));
                                    Usuarios.add(lUsuario);
                                    usuariosAdapter = new UsuariosAdapter(getApplicationContext(),
                                            R.layout.item_lista, Usuarios);
                                    lstUsuarios.setAdapter(usuariosAdapter);

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                queue.add(request);
            }
        });
    }
}
