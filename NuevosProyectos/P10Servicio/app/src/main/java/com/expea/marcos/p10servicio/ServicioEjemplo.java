package com.expea.marcos.p10servicio;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.util.Date;

public class ServicioEjemplo extends Service {
    private IBinder mBinder = new MyBinder();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Mi servicio inicio.", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < 3; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Mi servicio termino.", Toast.LENGTH_SHORT).show();
    }

    public String getFechaSistema() {
        Date fecha = new Date();
        return fecha.toString();
    }

    public class MyBinder extends Binder {
        ServicioEjemplo getService() {
            return ServicioEjemplo.this;
        }
    }
}
