﻿package EntidadAsesor;

public class clsPersonasPedido {
	private int fidPersonasPed;

	public final int getidPersonasPed() {
		return fidPersonasPed;
	}

	public final void setidPersonasPed(int value) {
		fidPersonasPed = value;
	}

	private String fNombreCompleto;

	public final String getNombreCompleto() {
		return fNombreCompleto;
	}

	public final void setNombreCompleto(String value) {
		fNombreCompleto = value;
	}

	private float fMontoPagar;

	public final float getMontoPagar() {
		return fMontoPagar;
	}

	public final void setMontoPagar(float value) {
		fMontoPagar = value;
	}

	private java.util.Date fFechaPago = new java.util.Date(0);

	public final java.util.Date getFechaPago() {
		return fFechaPago;
	}

	public final void setFechaPago(java.util.Date value) {
		fFechaPago = value;
	}

	private int fPagado;

	public final int getPagado() {
		return fPagado;
	}

	public final void setPagado(int value) {
		fPagado = value;
	}
}