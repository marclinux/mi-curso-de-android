package com.curso.expea.basedatossqlite;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by marcos on 27/08/2015.
 */
public class ContactosAdapter extends ArrayAdapter<clsContactos> {

    private int idResource;

    public ContactosAdapter(Context context, int resource,
                            ArrayList<clsContactos> objects) {
        super(context, resource, objects);
        idResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(idResource, parent, false);
        }
        clsContactos p = new clsContactos();
        p = getItem(position);
        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.txvId);
            TextView tt2 = (TextView) v.findViewById(R.id.txvNombreCompleto);
            TextView tt3 = (TextView) v.findViewById(R.id.txvNumeroTelefonico);
            if (tt1 != null) {
                tt1.setText(p.getId());
            }
            if (tt2 != null) {
                tt2.setText(p.getNombreCompleto());
            }
            if (tt3 != null) {
                tt3.setText(String.valueOf(p.getNumeroTelefonico()));
            }
        }
        return v;

    }
}
