package com.expea.curso.p14getproductovolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.expea.curso.p14getproductovolley.clases.clsProductos;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActividadPrincipal extends AppCompatActivity {
    EditText edvClave;
    Button btnMostrar;
    TextView txvId;
    TextView txvDescripcion;
    ImageView imvProducto;
    RequestQueue queue;
    String url = "http://10.1.52.233:8080/apiventas/getcatalagoarticulo.php?articulo=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        queue = Volley.newRequestQueue(getApplicationContext());
        iniciaVistas();
    }

    private void iniciaVistas() {
        edvClave = findViewById(R.id.edvClave);
        btnMostrar = findViewById(R.id.btnMostrar);
        txvId = findViewById(R.id.txvId);
        txvDescripcion = findViewById(R.id.txvDescripcion);
        imvProducto = findViewById(R.id.imvProducto);
        btnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String clave = edvClave.getText().toString();
                if(clave.length() == 0)
                    return;
                String urlConsulta = url + clave;
                StringRequest request = new StringRequest(StringRequest.Method.GET,
                        urlConsulta, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject producto = null;
                        try {
                            producto = new JSONObject(response);
                            if (producto != null) {
                                if(producto.getBoolean("estatus")) {
                                    clsProductos oProducto = new clsProductos();
                                    oProducto = Parse(producto);
                                    txvId.setText(String.valueOf(oProducto.getId()));
                                    txvDescripcion.setText(oProducto.getDescripcion());
                                    Picasso.get().load(oProducto.getRuta())
                                            .into(imvProducto);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), error.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                queue.add(request);
            }
        });

            }

    private clsProductos Parse(JSONObject producto) {
        try {
            clsProductos oProducto = new clsProductos();
            JSONArray catalogo = producto.getJSONArray("catalago");
            if(catalogo.length() > 0)
                oProducto.setRuta(catalogo.getJSONObject(0)
                        .getString("ruta"));
            JSONArray aDescripcion = producto.getJSONArray("descripcion");
            if(aDescripcion.length() > 0)
            {
                oProducto.setId(Integer.valueOf(aDescripcion.getJSONObject(0)
                .getString("id")));
                oProducto.setDescripcion(aDescripcion.getJSONObject(0)
                        .getString("descripcion"));
            }
            return oProducto;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
