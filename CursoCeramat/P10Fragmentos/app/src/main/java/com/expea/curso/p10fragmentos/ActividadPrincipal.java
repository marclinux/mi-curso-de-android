package com.expea.curso.p10fragmentos;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

public class ActividadPrincipal extends Activity implements EnviaMensaje{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
    }

    @Override
    public void EnviaDatos(String mensaje) {
        FragmentoDos Fragmento2 = (FragmentoDos) getFragmentManager().findFragmentById(R.id.Fragmento2);
        Fragmento2.RecibeDatos(mensaje);
    }
}
