﻿package EntidadAsesor;

public class clsClientes {
	private int fidCliente;

	public final int getidCliente() {
		return fidCliente;
	}

	public final void setidCliente(int value) {
		fidCliente = value;
	}

	private String fApellidosCliente;

	public final String getApellidosCliente() {
		return fApellidosCliente;
	}

	public final void setApellidosCliente(String value) {
		fApellidosCliente = value;
	}

	private String fNombresCliente;

	public final String getNombresCliente() {
		return fNombresCliente;
	}

	public final void setNombresCliente(String value) {
		fNombresCliente = value;
	}

	private String fColoniaCliente;

	public final String getColoniaCliente() {
		return fColoniaCliente;
	}

	public final void setColoniaCliente(String value) {
		fColoniaCliente = value;
	}

	private String fCiudadCliente;

	public final String getCiudadCliente() {
		return fCiudadCliente;
	}

	public final void setCiudadCliente(String value) {
		fCiudadCliente = value;
	}

	private String fCPCliente;

	public final String getCPCliente() {
		return fCPCliente;
	}

	public final void setCPCliente(String value) {
		fCPCliente = value;
	}

	private String fTelefonoCliente;

	public final String getTelefonoCliente() {
		return fTelefonoCliente;
	}

	public final void setTelefonoCliente(String value) {
		fTelefonoCliente = value;
	}

	private String fRFCCliente;

	public final String getRFCCliente() {
		return fRFCCliente;
	}

	public final void setRFCCliente(String value) {
		fRFCCliente = value;
	}

	private float fSaldoCliente;

	public final float getSaldoCliente() {
		return fSaldoCliente;
	}

	public final void setSaldoCliente(float value) {
		fSaldoCliente = value;
	}

	private String fFaxCliente;

	public final String getFaxCliente() {
		return fFaxCliente;
	}

	public final void setFaxCliente(String value) {
		fFaxCliente = value;
	}

	private String fEmailCliente;

	public final String getEmailCliente() {
		return fEmailCliente;
	}

	public final void setEmailCliente(String value) {
		fEmailCliente = value;
	}

	private int fTipo;

	public final int getTipo() {
		return fTipo;
	}

	public final void setTipo(int value) {
		fTipo = value;
	}

	private String fCalleCliente;

	public final String getCalleCliente() {
		return fCalleCliente;
	}

	public final void setCalleCliente(String value) {
		fCalleCliente = value;
	}

	private String fMunicipioCliente;

	public final String getMunicipioCliente() {
		return fMunicipioCliente;
	}

	public final void setMunicipioCliente(String value) {
		fMunicipioCliente = value;
	}

	private String fNoIntCliente;

	public final String getNoIntCliente() {
		return fNoIntCliente;
	}

	public final void setNoIntCliente(String value) {
		fNoIntCliente = value;
	}

	private String fNoExtCliente;

	public final String getNoExtCliente() {
		return fNoExtCliente;
	}

	public final void setNoExtCliente(String value) {
		fNoExtCliente = value;
	}
}