package com.expea.curso.p03parcelable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.expea.curso.p03parcelable.clases.clsUsuarios;

import java.util.ArrayList;
import java.util.List;

public class ActividadConsulta extends AppCompatActivity {
    TextView txvId;
    ArrayList<clsUsuarios> UsuariosLista;
    clsUsuarios Usuario;
    ListView lstUsuarios;
    UsuariosAdapter Adapatador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_consulta);
        txvId = findViewById(R.id.edtidConsulta);
        lstUsuarios = findViewById(R.id.lstUsuarios);
        Usuario = new clsUsuarios();
        UsuariosLista = new ArrayList<clsUsuarios>();
        UsuariosLista = (ArrayList<clsUsuarios>)
                Usuario.getUsuarios();
        Adapatador = new UsuariosAdapter(getApplicationContext(),
                R.layout.item_lista, (List) UsuariosLista);
        lstUsuarios.setAdapter(Adapatador);
        lstUsuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String mensaje = String.valueOf(UsuariosLista.get(position).getId());
                Intent intent = new Intent();
                intent.putExtra("idConsultado", mensaje);
                setResult(50, intent);
                finish();
            }
        });
    }
}
