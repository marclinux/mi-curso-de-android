package com.example.ariel.basededatos;

/**
 * Created by ariel on 27/08/2015.
 */
public class Contacto {
    private int id;
    private String nombre;
    private String tel;

    public Contacto(int id, String nombre, String tel) {
        this.id = id;
        this.nombre = nombre;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
