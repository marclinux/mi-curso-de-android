package com.ati.asesormovil;

import EntidadAsesor.clsPedidosMostrador;
import EntidadAsesor.clsProductosMovs;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MyListFragment extends Fragment {

	public clsPedidosMostrador pedido;

	ListView lstProductos;

	AdaptadorLista customListAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		pedido = new clsPedidosMostrador();
		clsProductosMovs prod = new clsProductosMovs();
		prod.setCantidad(2);
		prod.setClaveProducto("CLave");
		prod.setDescripcionProd("Producto1");
		prod.setPrecioUnit(20.3f);
		prod.setMontoIVA(1.2f);
		prod.setDescuento(0f);
		pedido.getDetalle().getLista().add(prod);
		pedido.getDetalle().getLista().add(prod);
		View rootView = inflater.inflate(R.layout.fragment_prods, container,
				false);
		lstProductos = (ListView) rootView.findViewById(R.id.lsvProductos);
		customListAdapter = new AdaptadorLista(getActivity(),
				R.layout.item_lista, pedido.getDetalle().getLista());
		lstProductos.setAdapter(customListAdapter);
		return rootView;
	}
}
