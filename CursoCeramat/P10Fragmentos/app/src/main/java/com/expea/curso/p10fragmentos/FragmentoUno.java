package com.expea.curso.p10fragmentos;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentoUno extends Fragment {
    View Vista;
    EditText edtNombre;
    Button btnAceptar;
    EnviaMensaje EMensaje;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Vista = inflater.inflate(R.layout.fragmento_uno, container, false);
        edtNombre = Vista.findViewById(R.id.edtNombre);
        btnAceptar = Vista.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje;
                mensaje = edtNombre.getText().toString();
                EMensaje.EnviaDatos(mensaje);
            }
        });
        return Vista;
    }

    @Override
    public void onAttach(Activity actividad) {
        super.onAttach(actividad);
        try {
            EMensaje = (EnviaMensaje) actividad;
        }
        catch (ClassCastException e) {
            throw new ClassCastException("Necesitas implementar la clase");
        }
    }
}
