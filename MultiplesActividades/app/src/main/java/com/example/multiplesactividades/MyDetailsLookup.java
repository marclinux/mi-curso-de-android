package com.example.multiplesactividades;

import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

class MyDetailsLookup extends ItemDetailsLookup {

    private final RecyclerView recyclerView;

    public MyDetailsLookup(RecyclerView lstUsuarios) {
        recyclerView = lstUsuarios;
    }

    @Nullable
    @Override
    public ItemDetails getItemDetails(@NonNull MotionEvent e) {
        View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (view != null) {
            RecyclerView.ViewHolder viewHolder = recyclerView.getChildViewHolder(view);
            if (viewHolder instanceof MiAdaptador.MiViewHolder) {
                return ((MiAdaptador.MiViewHolder) viewHolder).getItemDetails();
            }
        }
        return null;
    }
}
