package com.example.multiplesactividades;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class clsUsuarios implements Parcelable {
    private int idUsuario;
    private String Nombre;
    private String Login;
    private int Nivel;

    public clsUsuarios()
    {
        idUsuario = 0;
        Nombre = "";
        Login = "";
        Nivel = 0;
    }

    protected clsUsuarios(Parcel in) {
        idUsuario = in.readInt();
        Nombre = in.readString();
        Login = in.readString();
        Nivel = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idUsuario);
        dest.writeString(Nombre);
        dest.writeString(Login);
        dest.writeInt(Nivel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<clsUsuarios> CREATOR = new Creator<clsUsuarios>() {
        @Override
        public clsUsuarios createFromParcel(Parcel in) {
            return new clsUsuarios(in);
        }

        @Override
        public clsUsuarios[] newArray(int size) {
            return new clsUsuarios[size];
        }
    };

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public int getNivel() {
        int otro = Nivel;
        return Nivel;
    }

    public void setNivel(int nivel) {
        Nivel = nivel;
    }

    public void Obtener(String password)
    {
        setIdUsuario(1);
        setNombre("Marcos Hernandez");
        setNivel(99);
    }

    public ArrayList<clsUsuarios> ObtenTodos()
    {
        ArrayList<clsUsuarios> usuarios = new ArrayList<clsUsuarios>();
        clsUsuarios usuario = new clsUsuarios();
        usuario.setIdUsuario(1);
        usuario.setLogin("marcos");
        usuario.setNombre("Marcos Hernandez");
        usuario.setNivel(99);
        usuarios.add(usuario);
        usuario = new clsUsuarios();
        usuario.setIdUsuario(2);
        usuario.setLogin("pedro");
        usuario.setNombre("Pedro Perez");
        usuario.setNivel(40);
        usuarios.add(usuario);
        usuario = new clsUsuarios();
        usuario.setIdUsuario(3);
        usuario.setLogin("maria");
        usuario.setNombre("Maria Estrada");
        usuario.setNivel(70);
        usuarios.add(usuario);
        return usuarios;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
}
