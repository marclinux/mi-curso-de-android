package com.expea.curso.p03parcelable;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.expea.curso.p03parcelable.clases.clsUsuarios;

import java.util.List;

public class UsuariosAdapter extends
        ArrayAdapter<clsUsuarios> {
    private int layoutResourceId;
    public UsuariosAdapter(@NonNull Context context,
                           int resource,
                           @NonNull List<clsUsuarios> objects) {
        super(context, resource, objects);
        layoutResourceId = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,
                        @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(layoutResourceId, parent, false);
        }
        clsUsuarios Usuario = getItem(position);
        if(Usuario != null)
        {
            TextView txvId = convertView.findViewById(R.id.txvId);
            TextView txvLogin = convertView.findViewById((R.id.txvLogin));
            TextView txvNivel = convertView.findViewById(R.id.txvNivel);
            txvId.setText(String.valueOf(Usuario.getId()));
            txvLogin.setText(Usuario.getLogin());
            txvNivel.setText(String.valueOf(Usuario.getNivel()));
        }
        return convertView;
    }
}
