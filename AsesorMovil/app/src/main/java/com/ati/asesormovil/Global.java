package com.ati.asesormovil;

import com.android.volley.RequestQueue;

import EntidadAsesor.clsPedidosMostrador;
import EntidadAsesor.clsUsuarios;

public class Global {

	public static clsPedidosMostrador Pedido = new clsPedidosMostrador();
	
	public static boolean Nuevo = false;
	
	public static RequestQueue mRequestQueue;
	
	public static clsUsuarios UsuarioSesion = new clsUsuarios();
	
	public static String Servidor = "";
	
	public static String BaseDatos = "";
		
}
