package com.expea.curso.p08subprocesos;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ActividadPrincipal extends AppCompatActivity {
    TextView txvResultado;
    Button btnIniciar;
    ProgressBar prgbTarea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);
        txvResultado = findViewById(R.id.txvResultado);
        btnIniciar = findViewById(R.id.btnIniciar);
        prgbTarea = findViewById(R.id.prgbTarea);
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cadena = txvResultado.getText().toString();
                MiAsyncTask Tarea = new MiAsyncTask();
                Tarea.execute(cadena);
            }
        });
    }
    private class MiAsyncTask extends
            AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... strings) {
            String cadena = "";
            int inputLength = strings[0].length();
            for (int i = 1; i <= inputLength; i++) {
                cadena = cadena + strings[0]
                        .charAt(inputLength - i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
            }
            return cadena;
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
            prgbTarea.setProgress(values[0]);
        }
        @Override
        protected void onPostExecute(String s) {
            txvResultado.setText(s);
            prgbTarea.setVisibility(View.GONE);
        }
        @Override
        protected void onPreExecute() {
            prgbTarea.setVisibility(View.VISIBLE);
        }
    }
}


