package com.expea.curso.p10fragmentos;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentoDos extends Fragment{
    View Vista;
    TextView txvSaludo;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Vista = inflater.inflate(R.layout.fragmento_dos, container, false);
        txvSaludo = Vista.findViewById(R.id.txvSaludo);
        return Vista;
    }

    public void RecibeDatos(String mensaje)
    {
        txvSaludo.setText("Hola " + mensaje);
    }
}
